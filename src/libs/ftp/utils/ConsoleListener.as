package libs.ftp.utils
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	
	import libs.ftp.FTPClient;
	import libs.ftp.FTPCommand;
	import libs.ftp.events.FTPCommandEvent;

	public class ConsoleListener
	{
		private var ftpConnection:FTPClient;
		private var client_component:*;
		
		public function ConsoleListener (ftpConn:FTPClient, client:*)
		{
			ftpConnection = ftpConn;
			ftpConnection.addEventListener(Event.CONNECT, handleConnect);
			ftpConnection.addEventListener(Event.CLOSE, handleClose);
			ftpConnection.addEventListener(FTPCommandEvent.COMMAND, handleCommand);
			ftpConnection.addEventListener(FTPCommandEvent.REPLY, handleReply);
			ftpConnection.addEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			ftpConnection.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleSecurity);
			client_component = client;
		}
		
		public function dispose():void
		{
			ftpConnection.removeEventListener(Event.CONNECT, handleConnect);
			ftpConnection.removeEventListener(Event.CLOSE, handleClose);
			ftpConnection.removeEventListener(FTPCommandEvent.COMMAND, handleCommand);
			ftpConnection.removeEventListener(FTPCommandEvent.REPLY, handleReply);
			ftpConnection.removeEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			ftpConnection.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, handleSecurity);
		}
		
		private function handleConnect(evt:Event):void
		{
			client_component.Log ("Connected with", ftpConnection.host, ftpConnection.port);
		}
		
		private function handleClose(evt:Event):void
		{
			client_component.Log ("Closed connection", ftpConnection.host, ftpConnection.port);
		}
		
		private function handleCommand(evt:FTPCommandEvent):void
		{
			client_component.Log (evt.command.toString());
		}
		
		private function handleReply(evt:FTPCommandEvent):void
		{
			client_component.Log (evt.reply.toString());
		}
		
		private function handleIOError(evt:IOErrorEvent):void
		{
			client_component.Log ("IOError");
		}
		
		private function handleSecurity(evt:SecurityErrorEvent):void
		{
			client_component.Log (evt);
		}
		
		private function handleCommandReply(evt:FTPCommandEvent):void
		{
			client_component.Log ((evt.target as FTPCommand).toString());
			client_component.Log (evt.reply.toString());
		}

	}
}
