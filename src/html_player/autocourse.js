/* AutoCourse JavaScript Player version 0.2
   Ported from ActionScript by Rob Stewart in Sept 2011
   Compatible with AutoCourse editor version 0.5

   This pattern used was intended to maximize code similarity with the ActionScript player,
   making it easier to maintain two codebases. Porting is as simple as removing the variable types,
   changing the function declarations, putting 'this' in front of public properties and function calls,
   using jquery to browse the xml, and writing the output to HTML divs.

   Todo: Support more question types. More styling options, especially buttons and JQuery mobile.
   This code currently assumes a single assessment course, otherwise the scorm completion call needs
   to be handled accordingly.

   Copyright etrainu (c)2011. Modification or reuse are not permitted.*/
   
/* Language extensions ****************************************************************/

$.fn.disable = function(){ return this.each(function(){ if (typeof this.disabled != "undefined") this.disabled = true;});}
$.fn.enable = function(){ return this.each(function(){ if (typeof this.disabled != "undefined") this.disabled = false;});}
if (!Array.indexOf){ Array.prototype.indexOf = function(obj){ for(var i=0; i<this.length; i++){ if(this[i]==obj){ return i;}} return -1;}}
function Adopt (parent, child){ for (var prop in parent){ if (parent.hasOwnProperty(prop)){ child[prop] = parent[prop];}} return child;}
function IsDefined (variable){ return (typeof (window[variable]) == "undefined") ? false : true;}

/***************************************************************************************/

var AutoCourse = AutoCourse || {}

AutoCourse.CourseLoaded = function (root_xml)
{
	var course_xml = $(root_xml).find('course')[0];
	var ass_xml = $(course_xml).find('assessment')[0];

	AutoCourse.scorm = new AutoCourse.Scorm();
	AutoCourse.scorm.logging = true; // set to true for debugging only
	AutoCourse.scorm.CourseStart ();
	AutoCourse.assessment = new AutoCourse.Assessment (ass_xml);
	AutoCourse.assessment.AskQuestions();

	// optional - set page titles
	$('<div class="ass_title_css"></div>').html($(ass_xml).attr('name')).prependTo("#page_div");
	$('title').text ($(course_xml).find('info').children('title').text());
}

/* Assessment ****************************************************************************/

AutoCourse.Assessment = function (ass_xml)
{
	var slide_xml = ass_xml;
	var questions = [];
	var reserve_matrix = [];
	var reserve_questions = [];
	var current_question = null;
	var current_question_index = 0;
	var pass_threshold = Number($(ass_xml).attr('pass')) / 100;
	var question_count = 0;
	var number_correct = 0;
	var scorm_mode = false;
	var activity_mode = false;
	var question_start_time = 0;
	var activity_wrong_count = 0;

	this.Assessment = function (ass_xml)
	{
		var question_xml_list = $(ass_xml).find('question');
		for (var i = 0; i < question_xml_list.length; i++){
			var question_xml = question_xml_list[i];
			var question_type = $(question_xml).attr('type');
			var question = null;
			if (question_type == "multiple choice") question = new AutoCourse.QuestionMultipleChoice (question_xml);
			if (question){
				var assessment = this;
				question.activity_handler = function (question, option){ assessment.ActivityHandler (question, option);}
				var reserve = $(question_xml).attr('reserve');
				if (!reserve || reserve == ""){
					questions.push (question);
					reserve_matrix.push (new Array);
					question_count++;}
				else if (reserve == "any"){
					reserve_questions.push (question);}
				else if (reserve == "matrix"){
					if (reserve_matrix.length){
						var matrix_array = reserve_matrix[reserve_matrix.length-1];
						matrix_array.push (question);}}}}

		var mode = $(ass_xml).attr('mode');
		scorm_mode = (mode.indexOf ("scorm") != -1);
		activity_mode = (mode.indexOf ("activity") != -1);
		if (scorm_mode){
			AutoCourse.scorm.AssessmentStart (ass_xml);}
	}
	this.AskQuestions = function ()
	{
		this.DisplayCurrentQuestion();
		this.UpdateNavigation();
	}
	this.DisplayCurrentQuestion = function ()
	{
		activity_wrong_count = 0;
		$('<div id="question_div"></div>').appendTo("#assessment_div");
		current_question = questions[current_question_index];
		current_question.RenderQuestion();
		question_start_time = new Date().getTime();
		if (activity_mode)
			$('<div id="activity_div" class="activity_text_css"></div>').appendTo("#question_div");
	}
	this.NavigateForward = function ()
	{
		this.QuestionAnswered();
		if (current_question_index < questions.length - 1){
			this.RemoveQuestion();
			current_question_index++;
			this.DisplayCurrentQuestion();}
		this.UpdateNavigation();
	}
	this.NavigateBack = function ()
	{
		if (current_question_index > 0){
			this.RemoveQuestion();
			current_question_index--;
			this.DisplayCurrentQuestion();}
		this.UpdateNavigation();
	}
	this.UpdateNavigation = function ()
	{
		if (activity_mode){
			$('#button_next').disable();
			$('#button_back').disable();
			$('#button_submit').hide();}
		else {
			if (questions.length == 1){
				$('#button_next').disable();
				$('#button_back').disable();}
			else if (current_question_index == 0){
				$('#button_next').enable();
				$('#button_back').disable();}
			else if (current_question_index == questions.length - 1){
				$('#button_next').disable();
				$('#button_back').enable();}
			else {
				$('#button_next').enable();
				$('#button_back').enable();}
			if (current_question_index == questions.length - 1){ $('#button_submit').show();}
			else { $('#button_submit').hide();}}
		$('#button_retry').hide();
	}
	this.ShowAnswers = function()
	{
		$('#button_next').disable();
		$('#button_back').disable();

		$('<div id="results_div"></div>').appendTo("#assessment_div");
		$('<div class="results_title_css"></div>').html("Summary").appendTo("#results_div");
		$('<table id="results_table"></table>').appendTo("#results_div");
		for (var i=0; i<questions.length; i++){
			var question = questions[i];
			var is_correct = question.IsCorrect();
			if (is_correct) number_correct++;
			question.RenderResults();}

		var normalized_score = Number(number_correct) / Number (question_count);
		var percentage_score = Math.floor (normalized_score * 100)
		var passed = Boolean (normalized_score >= pass_threshold);
		var message_text_pass = "<p>Well done!</p><p>You have passed</p>";
		var message_text_fail = "<p>You scored $correct out of $total</p><p>$pass is required to pass<br>Please try again</p>";
		var message_text = "";
		if (passed){
			message_text = message_text_pass;
			if (scorm_mode){
				AutoCourse.scorm.AssessmentPassed (slide_xml, percentage_score);
				AutoCourse.scorm.CourseCompleted();}}
		else {
			var pass_string = String (Math.floor (pass_threshold * 100)) + "%";
			message_text = message_text_fail.replace ("$correct", number_correct).replace ("$total", question_count).replace ("$pass", pass_string);
			if (scorm_mode){
				AutoCourse.scorm.AssessmentFailed (slide_xml, percentage_score);}}

		 $('<div class="question_text_css"></div>').html(message_text).appendTo("#results_div");

		if (!passed){
			$('#button_retry').show();}
	}
	this.SubmitAssessment = function ()
	{
		this.QuestionAnswered();
		$('#button_submit').hide();
		this.RemoveQuestion();
		this.ShowAnswers();
	}
	this.QuestionAnswered = function ()
	{
		if (scorm_mode){
			if (current_question_index < questions.length){
				var question = questions[current_question_index];
				var is_correct = question.IsCorrect();
				var correct_response = question.CorrectResponse();
				var student_response = question.StudentResponse();
				var question_xml = question.question_xml;
				var latency = this.GetQuestionTime();
				AutoCourse.scorm.QuestionAnswered (question_xml, correct_response, student_response, latency, is_correct);}}
	}
	this.GetQuestionTime = function ()
	{
		var millisecs = new Date().getTime() - question_start_time;
		var hours = Math.floor (millisecs / 3600000);
		millisecs -= hours * 3600000;
		var minutes = Math.floor (millisecs / 60000);
		millisecs -= minutes * 60000;
		var seconds = millisecs / 1000;
		var latency = (hours > 9) ? String (hours) : ("0" + hours);
		latency += ":" + ((minutes > 9) ? String (minutes) : ("0" + minutes));
		latency += ":" + ((seconds > 9) ? seconds.toFixed(2) : ("0" + seconds.toFixed(2)));
		return latency;
	}
	this.RetryQuestions = function ()
	{
		this.RemoveResults ();
		$('#button_retry').hide();
		current_question_index = 0;
		for (var i=questions.length-1; i>=0; i--){
			var question = questions[i];
			if (question.IsCorrect()){
				questions.splice (i,1);
				reserve_matrix.splice (i,1);}
			else question.Reset();}
		for (i=0; i<questions.length; i++){
			question = questions[i];
			var question_number = question.question_number;
			var matrix_array = reserve_matrix[i];
			if (matrix_array.length){
				matrix_array.push (question);
				question = matrix_array.shift();
				question.question_number = question_number;
				questions[i] = question;}
			else {
				reserve_questions.push (question);
				question = reserve_questions.shift();
				question.question_number = question_number;
				questions[i] = question;}}
		this.AskQuestions ();
	}
	this.ActivityHandler = function (question, option)
	{
		if (activity_mode){
			var is_correct = question.IsCorrect();
			this.ShowActivityResponse (is_correct);
			if (is_correct){
				$('#button_next').enable();
				if (current_question_index == questions.length - 1){
					if (scorm_mode){
						Scorm.AssessmentPassed (slide_xml, 100);}}}
			else { // wrong
				$('#button_next').disable();}}
	}
	this.ShowActivityResponse = function (is_correct)
	{
		var activity_wrong = ["Wrong answer ... try again", "Still wrong ... keep trying"];
		var activity_correct = ["Correct ... well done!", "Correct again!<br><br>Congratulations, you have passed"];
		var correct_count = (questions.length > 1 && current_question_index == questions.length - 1) ? 1 : 0;
		var text = is_correct ? activity_correct[correct_count] : activity_wrong[activity_wrong_count];
		if (!is_correct && activity_wrong_count < activity_wrong.length - 1) activity_wrong_count++;
		$('#activity_div').html (text);
	}
	this.RemoveQuestion = function ()
	{
		$('#question_div').remove();
	}
	this.RemoveResults = function ()
	{
		$('#results_div').remove();
	}
	this.Assessment (ass_xml);
}

/* Question base *********************************************************************/

AutoCourse.Question = function (ques_xml)
{
	this.question_xml = ques_xml;
	this.activity_handler = null;
	this.RenderQuestion = function (){}
	this.RenderResults = function (){}
	this.IsCorrect = function (){ return false;}
	this.StudentResponse = function (){}
	this.CorrectResponse = function (){}
	this.Reset = function (){}
}
/* QuestionMultipleChoice ***************************************************************/

AutoCourse.QuestionMultipleChoice = function (ques_xml)
{
	Adopt (new AutoCourse.Question (ques_xml), this);
	var question_text = $(ques_xml).children('text').text();
	var question_number = String($(ques_xml).attr('ord')).replace(/[^0-9]/g, "");
	var question_options = [];
	var answer_count = 0;

	this.QuestionMultipleChoice = function ()
	{
		var option_xml_list = $(ques_xml).find('option');
		for (var i=0; i<option_xml_list.length; i++){
			var option_xml = option_xml_list[i];
			var option = new AutoCourse.OptionMultipleChoice(option_xml);
			if (option.IsCorrectAnswer()) answer_count++;
			question_options.push(option);}
	}
	this.RenderQuestion = function ()
	{
		$('<div class="question_title_css"></div>').html("Question " + question_number).appendTo("#question_div");
		$('<div class="question_text_css"></div>').html(question_text).appendTo("#question_div");

		if (answer_count > 1){
			var count_text = "( Tick " + answer_count + " answers )";
			$('<div class="help_text_css"></div>').html(count_text).appendTo("#question_div");}

		$('<table id="options_table"></table>').appendTo("#question_div");
		for (var i=0; i<question_options.length; i++) {
			var option = question_options[i];
			var tag = option.RenderOption();
			tag.click ((function(_question,_option){ return function(){ return _question.OptionClicked(_option); }})(this, option));}
	}
	this.RenderResults = function ()
	{
		var image_url = this.IsCorrect() ? "result_tick.png" : "result_cross.png";
		var row = $('<tr></tr>').appendTo ("#results_table");
		var image = $('<img src="' + image_url + '" width="22" height="22"/>').supersleight();
		$('<td></td>').html('Question ' + question_number).appendTo (row);
		$('<td></td>').html(image).appendTo (row);
	}
	this.OptionClicked = function (option)
	{
		if (answer_count == 1){
			this.UncheckAll();
			option.Check();
			if (this.activity_handler){
				this.activity_handler (this, option);}}
		else {
			option.Toggle();
			if (this.CountResponses() >= answer_count){
				if (this.activity_handler){
					this.activity_handler (this, option);}}}
	}
	this.UncheckAll = function ()
	{
		for (var i=0; i<question_options.length; i++){
			var option = question_options[i];
			option.Uncheck();}
	}
	this.IsCorrect = function ()
	{
		for (var i=0; i<question_options.length; i++){
			var option = question_options[i];
			if (!option.IsCorrect()) return false;}
		return true;
	}
	this.StudentResponse = function ()
	{
		var response = "";
		for (var i=0; i<question_options.length; i++){
			var option = question_options[i];
			if (response.length) response += ",";
			response += option.IsChecked() ? "1":"0";}
		return response;
	}
	this.CorrectResponse = function ()
	{
		var response = "";
		for (var i=0; i<question_options.length; i++){
			var option = question_options[i];
			if (response.length) response += ",";
			response += option.IsCorrectAnswer() ? "1":"0";}
		return response;
	}
	this.CountResponses = function ()
	{
		var count = 0;
		for (var i=0; i<question_options.length; i++){
			var option = question_options[i];
			if (option.IsChecked()) count++;}
		return count;
	}
	this.Reset = function (){ this.UncheckAll(); }

	this.QuestionMultipleChoice ();
}

/* OptionMultipleChoice ******************************************************************/

AutoCourse.OptionMultipleChoice = function (option_xml)
{
	var option_checked = false;
	var correct_answer = ($(option_xml).attr('answer') == "true") ? true : false;
	var option_text = $(option_xml).text();
	var option_id = "option" + $(option_xml).attr('ord');

	this.IsChecked = function (){ return option_checked;}
	this.Check = function (){ $("#"+option_id).attr('checked', option_checked = true);}
	this.Uncheck = function (){ $("#"+option_id).attr('checked', option_checked = false);}
	this.Toggle = function (){ if (option_checked) this.Uncheck(); else this.Check();}
	this.IsCorrect = function (){ return option_checked == correct_answer; }
	this.IsCorrectAnswer = function (){ return correct_answer; }
	this.RenderOption = function ()
	{
		var option_div = '<tr class="option_text_css"></tr>';
		var option_checkbox = '<td style="vertical-align:top"><input type="checkbox" id="' + option_id + '" /></td>';
		var option_text_cell = '<td class="show_hand">' + option_text + '</td>';
		var tag = $(option_div).html (option_checkbox + option_text_cell).appendTo("#options_table");
		if (option_checked) this.Check();
		return tag;
	}
}

/* Scorm *********************************************************************************/

AutoCourse.Scorm = function ()
{
	this.enable = true;
	this.logging = false;

	var interactions = [];
	var objectives = [];
	var course_xml = null;

	this.CourseStart = function ()
	{
		this.LMSSetValue ("cmi.core.lesson_status", "incomplete");
	}
	this.AssessmentStart = function (ass_xml)
	{
		var auid = this.GetUniqueID (ass_xml);
		var index = this.GetObjectiveIndex (auid);
		var base = "cmi.objectives." + index;
		this.LMSSetValue (base + ".id", auid);
		this.LMSSetValue (base + ".score.min", "0");
		this.LMSSetValue (base + ".score.max", "100");
		this.LMSSetValue (base + ".status", "incomplete");
		objectives[index].status = "started";
	}
	this.QuestionAnswered = function (ques_xml, correct_response, student_response, latency, result)
	{
		var ass_xml = $(ques_xml).parent();
		var quid = this.GetUniqueID (ques_xml);
		var auid = this.GetUniqueID (ass_xml);
		var index = this.GetInteractionIndex (quid);
		var base = "cmi.interactions." + index;
		this.LMSSetValue (base + ".id", quid);
		this.LMSSetValue (base + ".objectives.0.id ", auid);
		this.LMSSetValue (base + ".type", this.TranslateQuestionType ($(ques_xml).attr('type')));
		this.LMSSetValue (base + ".correct_responses.0.pattern", correct_response);
		this.LMSSetValue (base + ".student_response", student_response);
		this.LMSSetValue (base + ".result", result ? "correct" : "wrong");
		this.LMSSetValue (base + ".latency", latency);
	}
	this.AssessmentPassed = function (ass_xml, score)
	{
		var auid = this.GetUniqueID (ass_xml);
		var index = this.GetObjectiveIndex (auid);
		var base = "cmi.objectives." + index;
		this.LMSSetValue (base + ".score.raw", String (score));
		this.LMSSetValue (base + ".status", "passed");
		objectives[index].status = "passed";
		this.LMSCommit();
	}
	this.AssessmentFailed = function (ass_xml, score)
	{
		var auid = this.GetUniqueID (ass_xml);
		var index = this.GetObjectiveIndex (auid);
		var base = "cmi.objectives." + index;
		this.LMSSetValue (base + ".score.raw", String (score));
		this.LMSSetValue (base + ".status", "failed");
		objectives[index].status = "failed";
		this.LMSCommit();
	}
	this.CourseCompleted = function ()
	{
		this.LMSSetValue ("cmi.core.lesson_status", "completed");
	}
	this.LMSSetValue = function (property, value)
	{
		if (!this.enable) return;
		if (IsDefined('LMSSetValue')) LMSSetValue (property, value);
		this.Log ("LMSSetValue (\"" + property + "\", \"" + value + "\")");
	}
	this.LMSCommit = function ()
	{
		if (!this.enable) return;
		if (IsDefined('LMSCommit')) LMSCommit ("");
		this.Log ("LMSCommit()");
	}
	this.GetObjectiveIndex = function (id)
	{
		for (var index=0; index<objectives.length; index++){
			if (objectives[index].id == id) break;}
		if (index == objectives.length){
			objectives.push ({ id:id, status:"" });}
		return index;
	}
	this.GetInteractionIndex = function (id)
	{
		var index = interactions.indexOf (id);
		if (index == -1){ index = interactions.length; interactions.push (id);}
		return index;
	}
	this.GetUniqueID = function (xml)
	{
		var uid = "";
		var xml_parent = $(xml).parent();
		var info_list = $(xml_parent).children('info');
		if (!info_list.length){
			uid = this.GetUniqueID (xml_parent);}
		var ord = $(xml).attr('ord');
		if (ord){
			if (uid.length) uid += "_";
			uid += ord;}
		return uid;
	}
	this.TranslateQuestionType = function (rob_type)
	{
		var scorm_type = "other";
		var rob_types = ["multiple choice", "match text text", "match text image", "drag drop text"];
		var scorm_types = ["choice", "matching", "matching", "dragdrop"];
		var i = rob_types.indexOf (rob_type);
		if (i != -1){ scorm_type = scorm_types[i];}
		return scorm_type;
	}
	this.Log = function (message)
	{
		if (this.logging && IsDefined('console'))
			console.log (message);
	}
}
