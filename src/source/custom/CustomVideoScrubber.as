package source.custom
{
import flash.display.*;
import flash.events.*;
import flash.filters.*;
import flash.geom.*;

import mx.core.*;
import mx.effects.Move;
import mx.events.SliderEvent;
import mx.events.TweenEvent;

/*
	Used by CustomVideo
	Based on FXProgressSlider
*/

public class CustomVideoScrubber extends UIComponent
{
	private var slider_event:SliderEvent;
	private var slider_offset:Number;
	private var thumb_pressed:Boolean = false;	

	protected var track:Sprite;
	protected var thumb:Sprite;
	protected var thumb_radius:Number = 6;

	private var _maximum:Number = 100;
    public function get maximum():Number { return _maximum;}
    public function set maximum(value:Number):void
    {
        _maximum = value;
        invalidateProperties();
        invalidateDisplayList();
    }

	private var _value:Number = 0;
	private var valueChanged:Boolean = false;
	public function set value(value:Number):void
	{
		_value = value;
		valueChanged = true;
		if (value == 0)
			thumb.x = thumb.width/2;
		invalidateProperties();
		invalidateDisplayList();
	}
	public function get value():Number
	{
		return (thumb.x - thumb.width/2)/((unscaledWidth - thumb.width)/100)*(maximum/100);
	}
	private function get boundMin():Number
	{
		return thumb.width/2
	}
	private function get boundMax():Number
	{
		return Math.max(thumb.width/2, unscaledWidth - thumb.width/2);
	}

	override protected function createChildren():void
	{
		super.createChildren();

		systemManager.addEventListener (MouseEvent.MOUSE_UP, onMouseUp);

		if (!track)
        {
            track = new Sprite();
            addChild (track); 
        }

		track = new Sprite;
		addChild (track);

		thumb = new Sprite;
		var mat:Matrix = new Matrix();
  		mat.createGradientBox (16, 16, 0, -10, -10);
		thumb.graphics.beginGradientFill ("radial", [0x8C8884, 0x3F3D3B], [1, 1], [0, 255], mat);
		thumb.graphics.drawCircle (1, 2, thumb_radius);
		addChild (thumb);

		thumb.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		thumb.addEventListener(MouseEvent.MOUSE_OVER, function ():void { thumb.filters = [new GlowFilter (0xFFFFFF, 0.8, 6, 6, 2, 2)];});
		thumb.addEventListener(MouseEvent.MOUSE_OUT, function ():void { thumb.filters = [];});
		thumb.buttonMode = true;
	}

	override protected function updateDisplayList (unscaledWidth:Number, unscaledHeight:Number):void
    {
        super.updateDisplayList(unscaledWidth, unscaledHeight);

		if (valueChanged)
        {
        	thumb.x = Math.round ((_value/_maximum) * (unscaledWidth - thumb.width) + thumb.width/2);
        	valueChanged = false;
        }

		track.graphics.clear();
		track.graphics.lineStyle (1, 0x000000);
		track.graphics.moveTo (0, 0);
		track.graphics.lineTo (unscaledWidth, 0);
		track.graphics.lineStyle (1, 0x666666);
		track.graphics.moveTo (1, 1);
		track.graphics.lineTo (unscaledWidth+1, 1);

		track.y = unscaledHeight/2-1;
		thumb.y = track.y;
    }
    
    override protected function measure():void
	{
		super.measure();
		
		measuredWidth = 200;
		measuredHeight = 13;
	}
	
	private function onMouseDown(event:MouseEvent):void
	{
		systemManager.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove, true);

		slider_offset = event.localX;

		thumb_pressed = true;

		slider_event = new SliderEvent(SliderEvent.THUMB_PRESS);
        slider_event.value = value;
        dispatchEvent(slider_event);
	}
	
	private function onMouseUp(event:MouseEvent):void
	{
		systemManager.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove, true);
		
		if(thumb_pressed)
		{
			slider_event = new SliderEvent(SliderEvent.THUMB_RELEASE);
	        slider_event.value = value;
	        dispatchEvent(slider_event);
	        
	        slider_event = new SliderEvent(SliderEvent.CHANGE);
	        slider_event.value = value;
	        dispatchEvent(slider_event);
	        
	        thumb_pressed = false;
		}
	}
	
	private function onMouseMove(event:MouseEvent):void
	{
		var pt:Point = new Point(event.stageX, event.stageY);
		pt = globalToLocal(pt);
			
		thumb.x = Math.min(Math.max(pt.x - slider_offset, boundMin), boundMax);
		
		slider_event = new SliderEvent(SliderEvent.THUMB_DRAG);
        slider_event.value = value;
        dispatchEvent(slider_event);
        
        invalidateDisplayList();
	}
}
}