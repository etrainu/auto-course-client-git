package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	
	import source.general.*;

	public class QuestionMatchTextText extends QuestionMatch
	{
		public function QuestionMatchTextText (ques_xml:XML):void
		{
			super (ques_xml);

			var area_width:int = options_width ? options_width : Course.page_width;

			var option_xml_list:XMLList = ques_xml.option;
			// find max width
			var max_width_left:int = 0;
			var max_width_right:int = 0;
			for each (var option_xml:XML in option_xml_list){
				var left_text:String = option_xml.left;
				var right_text:String = option_xml.right;
				var sprite_left:Sprite = OptionMatchText.MakePanel (0, 0, left_text);
				var sprite_right:Sprite = OptionMatchText.MakePanel (0, 0, right_text);
				max_width_left = Math.max (max_width_left, sprite_left.width);
				max_width_right = Math.max (max_width_right, sprite_right.width);}

			// algorithm for widths and heights
			if (max_width_left + max_width_right < area_width - Option.box_gap){
				left_width = max_width_left + (area_width - (max_width_left + max_width_right + Option.box_gap)) / 2;
				right_width = area_width - left_width - Option.box_gap;}
			else {
				left_width = max_width_left / (max_width_left + max_width_right) * (area_width * 2 / 3) + (area_width / 6);
				right_width = area_width - left_width - Option.box_gap;}

			// find max height
			var max_height:int = 0;
			for each (option_xml in option_xml_list){
				left_text = option_xml.left;
				right_text = option_xml.right;
				max_height = Math.max (max_height, OptionMatchText.MakePanel (left_width, 0, left_text).height);
				max_height = Math.max (max_height, OptionMatchText.MakePanel (right_width, 0, right_text).height);}
			box_height = max_height;

			for each (option_xml in option_xml_list){
				left_text = option_xml.left;
				right_text = option_xml.right;
				var option_left:OptionMatchText = new OptionMatchText (option_xml, left_width, box_height, left_text, DraggingLeft, Activity);
				var option_right:OptionMatchText = new OptionMatchText (option_xml, right_width, box_height, right_text, DraggingRight, Activity);
				options_left.push (option_left);
				options_right.push (option_right);}

			ShuffleRightColumn ();
		}
	}
}