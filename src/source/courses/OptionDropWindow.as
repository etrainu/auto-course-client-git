package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	
	import source.custom.*;
	import source.general.*;

	public class OptionDropWindow extends Option
	{
		public var box_width:int;
		public var box_height:int;
		public var box_label:String;
		public var text_sprite:CustomSprite;
		public var drop_array:Array = [];
		public var dragtion_xpos:int = 8;
		public var dragtion_ypos:int;
		public static var min_height:int = 68;

		public function OptionDropWindow (xml:XML, _width:int)
		{
			super (xml);
			box_width = _width;
			box_height = min_height;
			box_label = String (xml.text);
			Render ();
		}
		public function Render () : void
		{
			while (numChildren)
				this.removeChildAt(0);

			var box : Sprite = new Sprite;
			text_sprite = GeneralText.MakeText (box_label, text_size, text_color, 0, 0, box_width-horz_margin*2);
			text_sprite.x = horz_margin;// + (box_width - text_sprite.width) / 2;
			text_sprite.y = vert_margin;
			var label_height:int = text_sprite.height + vert_margin * 2;

			box.graphics.beginFill (background_color, background_alpha);
			box.graphics.drawRoundRectComplex (0, 0, box_width, label_height, corner_radius, corner_radius, 0, 0)
			box.graphics.endFill ();
			
			box_height = label_height + Option.box_gap * 2;
			for each (var dragtion:OptionDragText in drop_array){
				box_height += dragtion.box_height + Option.box_gap + 1;}
			if (box_height < min_height) box_height = min_height; 

			box.graphics.lineStyle (1, border_color, border_alpha, true);
			box.graphics.drawRoundRect (0, 0, box_width, box_height, corner_radius, corner_radius);
			box.graphics.moveTo (0, label_height);
			box.graphics.lineTo (box_width, label_height);
			box.buttonMode = true;
			box.addChild (text_sprite);
			//this.addEventListener (MouseEvent.MOUSE_DOWN, OnMouseDown);
			addChild (box);

			dragtion_ypos = label_height + 8;
		}
	}
}
