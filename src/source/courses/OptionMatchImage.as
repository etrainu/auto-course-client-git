package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	
	import source.custom.*;
	import source.general.*;

	public class OptionMatchImage extends OptionDrag
	{
		public var margin:int = 2;
		public var image_loader:CustomLoader;

		public function OptionMatchImage (xml:XML, _width:int, _height:int, url:String, move:Function, drop:Function)
		{
			super (xml, _width, _height, move, drop);
			var box : Sprite = new Sprite;
			image_loader = GeneralLoader.LoadSizedImage (url, box_width-margin*2, box_height-margin*2, ImageLoaded);
			image_loader.x = margin;
			image_loader.y = margin;
			box.graphics.lineStyle (1, border_color, border_alpha, true);
			box.graphics.beginFill (background_color, background_alpha);
			box.graphics.drawRect (0, 0, box_width, box_height);
			box.buttonMode = true;
			box.addChild (image_loader);
			this.addEventListener (MouseEvent.MOUSE_DOWN, OnMouseDown);
			addChild (box);
		}
		public function ImageLoaded (image:DisplayObject):void
		{
			image.x = (box_width - margin*2 - image.width) / 2;
			image.y = (box_height - margin*2 - image.height) / 2;
		}
	}
}
