package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import source.general.*;

	public class QuestionDragDrop extends Question
	{
		public var options_drag:Array = [];
		public var options_drop:Array = [];
		public var drop_width:int;
		public var drag_xpos:int;
		public var drag_ypos:int;
		public var drop_xpos:int;
		public var drop_ypos:int;

		public function QuestionDragDrop (ques_xml:XML):void
		{
			super (ques_xml);

			var drop_count:int = 0;
			var option_xml_list:XMLList = ques_xml.option;
			for each (var option_xml:XML in option_xml_list){
				var type:String = option_xml.@type;
				if (type == "drop") drop_count++;}

			var area_width:int = options_width ? options_width : Course.page_width;
			drop_width = (area_width - Option.box_gap * (drop_count - 1)) / drop_count;

			for each (option_xml in option_xml_list){
				type = option_xml.@type;
				if (type == "drag"){
					var option_drag:OptionDragText = new OptionDragText (option_xml, DragHandler, DropHandler);
					options_drag.push (option_drag);}
				else if (type == "drop"){
					var option_drop:OptionDropWindow = new OptionDropWindow (option_xml, drop_width);
					options_drop.push (option_drop);}}

			// shuffle the drag boxes
			var count:int = 0;
			var length:int = options_drag.length;
			while (count < length && length > 1){
				var i:int = Math.random() * length;
				var j:int = Math.random() * length;
				if (i != j){
					var temp:* = options_drag[i];
					options_drag[i] = options_drag[j];
					options_drag[j] = temp;
					count++;}}
		}
		public override function RenderQuestion ():Sprite
		{
			var question_sprite:Sprite = new Sprite;
			var head_sprite:Sprite = GetQuestionHead();
			question_sprite.addChild (head_sprite);

			drop_xpos = 0;
			drop_ypos = head_sprite.height + text_gap;
			if (options_xpos != 0) drag_xpos = drop_xpos = options_xpos;
			if (options_ypos != 0) drop_ypos = options_ypos;

			LayoutDropWindows (question_sprite);
			LayoutDragBoxes (question_sprite);
			return question_sprite;
		}
		public function LayoutDropWindows (question_sprite:Sprite=null):void
		{
			var xpos:int = drop_xpos;
			var max_height:int = 0;
			for each (var droption:OptionDropWindow in options_drop){
				droption.x = xpos;
				droption.y = drop_ypos;
				droption.Render ();
				xpos += drop_width + Option.box_gap;
				if (question_sprite){
					question_sprite.addChild (droption);}
				max_height = Math.max (max_height, droption.height);}
			drag_ypos = drop_ypos + max_height + Option.box_gap * 2;
		}
		public function LayoutDragBoxes (question_sprite:Sprite=null):void
		{
			var xpos:int = drag_xpos;
			var ypos:int = drag_ypos;
			var area_width:int = options_width ? options_width : Course.page_width;
			for each (var dragtion:OptionDragText in options_drag){
				if (!dragtion.dragging){
					if (dragtion.drop_window){
						dragtion.ShowInWindow();}
					else {
						if (xpos + dragtion.box_width > drag_xpos + area_width){
							xpos = drag_xpos;
							ypos += dragtion.box_height + Option.box_gap;}
						dragtion.ShowAt (xpos, ypos);
						xpos += dragtion.box_width + Option.box_gap;}
					if (question_sprite){
						question_sprite.addChild (dragtion);}}}
		}
		public function DragHandler (option:Option):void
		{
			var dragtion:OptionDragText = OptionDragText (option);
			dragtion.ClearDroppings ();
			for each (var droption:OptionDropWindow in options_drop){
				var fudge_x:int = dragtion.width / 2.5;
				var fudge_y:int = dragtion.height / 2.5;
				if (dragtion.x >= droption.x - fudge_x &&
					dragtion.y >= droption.y - fudge_y &&
					dragtion.x + dragtion.width <= droption.x + droption.width + fudge_x &&
					dragtion.y + dragtion.height <= droption.y + droption.height + fudge_y){
					dragtion.drop_window = droption;
					droption.drop_array.push (dragtion);}}		
			LayoutDropWindows ();
			LayoutDragBoxes ();
		}
		public function DropHandler (option:Option):void
		{
			DragHandler (option);
			Activity (option);
		}
		public override function OnEnterFrame (event:Event):void
		{
			for (var i:int = 0; i < options_drag.length; i++){
				var option:OptionDrag = options_drag[i];
				option.OnEnterFrame (event);}
		}
		public override function Cheat():void
		{
			for each (var droption:OptionDropWindow in options_drop){
				droption.drop_array = [];}
			for each (var dragtion:OptionDragText in options_drag){
				for each (droption in options_drop){
					if (droption.option_group == dragtion.option_group){
						dragtion.drop_window = droption;
						droption.drop_array.push (dragtion);
						break;}}}
			LayoutDropWindows ();
			LayoutDragBoxes ();
			super.Cheat();
		}
		public override function IsCorrect ():Boolean
		{
			for each (var dragtion:OptionDragText in options_drag){
				if (!dragtion.drop_window || dragtion.drop_window.option_group != dragtion.option_group){
					return false;}}
			return true;
		}
		public override function StudentResponse():String
		{
			var response:String = "";
			for each (var droption:OptionDropWindow in options_drop){
				if (response.length) response += ",";
				response += droption.option_group;
				for each (var dragtion:OptionDragText in options_drag){
					if (dragtion.drop_window == droption){
						response += "." + dragtion.option_ord;}}}
			return response;
		}
		public override function CorrectResponse():String
		{
			var response:String = "";
			for each (var droption:OptionDropWindow in options_drop){
				if (response.length) response += ",";
				response += droption.option_group;
				for each (var dragtion:OptionDragText in options_drag){
					if (droption.option_group == dragtion.option_group){
						response += "." + dragtion.option_ord;}}}
			return response;
		}
		public override function Reset():void
		{
			for each (var dragtion:OptionDragText in options_drag){
				dragtion.drop_window = null;}
			for each (var droption:OptionDropWindow in options_drop){
				droption.drop_array = [];}
		}
	}
}
