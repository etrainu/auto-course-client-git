package source.courses
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;

	public class Config extends EventDispatcher
	{
		[Bindable] public var username:String = "Developer " + Math.floor (Math.random() * 900 + 100);
		[Bindable] public var password:String = "";
		[Bindable] public var course_counter:int = 1;
		[Bindable] public var course_folder:String = "";
		[Bindable] public var course_server:String = "http://localhost:8080/AutoCourseServer";
		[Bindable] public var course_search:String = "";
		[Bindable] public var course_file:String = "";
		[Bindable] public var sound_search:String = "q#.mp3";
		public var course_xml:XML = null;

		public function Config (folder:String)
		{
			course_folder = folder;
		}
		public function Copy (source:Object):void
		{
			for (var property_name:* in source){
				var data:Object = source[property_name];
				this[property_name] = data;}
		}
	}
}
