package source.courses
{
	import flash.net.*;
	import flash.events.*;
	import source.courses.*;
	import source.custom.*;
	import source.general.*;
	import mx.collections.*;
	import mx.controls.*;
	import mx.core.*;
	import mx.events.*;

	public class Server
	{
		public static function LoadServerStatus () : void
		{
			var editor:AutoCourse = AutoCourse.editor;
			var loader : CustomURLLoader = new CustomURLLoader;
			var request:URLRequest = new URLRequest (editor.config.course_server + "/status");
			loader.addEventListener (IOErrorEvent.IO_ERROR , function (event:Event):void { ServerConnectionError (event.toString())});
			loader.addEventListener (Event.COMPLETE, function (event:Event):void { ShowServerStatus (String (loader.data))});
			try { loader.load(request);} catch (error:Error){ ServerConnectionError (error.toString());}
			ShowServerStatus ("Connecting to server ..."); 
		}
		public static function SearchServerCourses () : void
		{
			var editor:AutoCourse = AutoCourse.editor;
			var search_string:String = editor.config.course_search.toLowerCase();
			var search_array:Array = search_string.split (/\W+/);
			if (search_array.length == 1 && search_array[0] == ""){
				ShowServerStatus ("Please enter one or more search terms"); 
				return;}
			var search_xml:String = "<search>";
			for each (var term:String in search_array)
			search_xml += "<term>" + term + "</term>";
			search_xml += "</search>";
			var search_target:String = String(editor.option_search_in.selectedItem).toLowerCase();
			var loader : CustomURLLoader = new CustomURLLoader;
			var url:String = editor.config.course_server + "/search/" + search_target;
			var request:URLRequest = new URLRequest (url);
			request.method = URLRequestMethod.POST;
			request.data = search_xml;
			request.contentType = "text/xml";
			loader.addEventListener (IOErrorEvent.IO_ERROR , function (event:Event):void { ServerConnectionError (event.toString())});
			loader.addEventListener (Event.COMPLETE, ServerSearchLoaded);
			try { loader.load(request);} catch (error:Error){ ServerConnectionError (error.toString());}
		}
		public static function ServerSearchLoaded (event:Event):void
		{
			var editor:AutoCourse = AutoCourse.editor;
			var loader : CustomURLLoader = CustomURLLoader (event.target);
			var search_xml:XML = XML (loader.data);
			var search_xml_list:XMLList = search_xml.map;
			for each (var map_xml:XML in search_xml_list){
				var entry_xml_list:XMLList = map_xml.entry;
				for each (var entry_xml:XML in entry_xml_list){
					entry_xml.setName (entry_xml.@key);
					delete entry_xml.@key;}}
			AutoCourse.search_array = new XMLListCollection (search_xml_list);
			if (search_xml_list.length() == 0) ShowServerStatus ("No matching courses found"); 
			else if (search_xml_list.length() > 100) ShowServerStatus ("More than 100 courses found<br><br>Please narrow down your search");
			else ShowServerStatus (String (search_xml_list.length()) + " matching courses found", 1);
		}
		public static function ShowServerStatus (message:String, index:int=0) : void
		{
			var editor:AutoCourse = AutoCourse.editor;
			if (message.indexOf ("<body") == -1){
				message = "<body style=\"font-size:12px\">" + message + "</body>";}
			editor.server_status.htmlText = message;
			editor.server_tabs.selectedIndex = index;
		}
		public static function LoadServerSelection (selection:XML) : void
		{
			var editor:AutoCourse = AutoCourse.editor;
			var loader : CustomURLLoader = new CustomURLLoader;
			var url:String = editor.config.course_server + "/request/selected";
			var request:URLRequest = new URLRequest (url);
			request.method = URLRequestMethod.POST;
			request.data = selection;
			request.contentType = "text/xml";
			loader.addEventListener (IOErrorEvent.IO_ERROR , function (event:Event):void { ServerConnectionError (event.toString())});
			loader.addEventListener (Event.COMPLETE, ServerSelectionLoaded);
			try { loader.load(request);} catch (error:Error){ ServerConnectionError (error.toString());}
			editor.load_course_status.text = "Loading " + String (selection.title) + " ... ";
		}
		public static function ServerSelectionLoaded (event:Event):void
		{
			var editor:AutoCourse = AutoCourse.editor;
			editor.CloseCourse ();
			editor.load_course_status.text = "";
			var loader : CustomURLLoader = CustomURLLoader (event.target);
			var loaded_xml:XML = XML (loader.data);
			editor.course_xml = loaded_xml;
			editor.CreateContentFolder ();

			var wddx_list:XMLList = loaded_xml..wddx;
			for each (var wddx:XML in wddx_list)
			{
				var type:String = String (wddx.@type)
				var title:String = type.charAt(0).toLocaleUpperCase() + type.slice(1);
				var wddx_content:XML = XML (unescape (wddx.text()));
				var variables:XMLList = wddx_content.data.struct.child("var");

				if (type == "text"){
					if (variables.(@name == "VALUE").length()){
						var html:String = variables.(@name == "VALUE")[0].string;
						if (html.search (/<.*?>/) != -1){ type = "html"; title = "HTML";} 
						wddx.text = html;}}
				else {
					if (variables.(@name == "VALUE").length()){
						var url:String = variables.(@name == "VALUE")[0].string;
						wddx.@url = url;}}

				wddx.setName ("element");
				wddx.@name = title;
				wddx.@type = type;
				delete wddx.text()[0];
			}

			editor.course_enable = true;
			editor.StoreCourse ();
			editor.UpdateStageList ();
			editor.UpdateChangeLog ();
		}
		public static function ServerConnectionError (message:String) : void
		{
			ShowServerStatus ("<p>Cannot connect to AutoCourse webserver</p><p>" + message + "</p>");
		}
	}
}
