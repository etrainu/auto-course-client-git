package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	
	import source.custom.*;
	import source.general.*;

	public class OptionDragText extends OptionDrag
	{
		public var text_sprite:CustomSprite;
		public var drop_window:OptionDropWindow;

		public function OptionDragText (xml:XML, move:Function, drop:Function)
		{
			var box : Sprite = new Sprite;
			var text:String = String (xml.text);
			text_sprite = GeneralText.MakeText (text, text_size, text_color, 0, 0);
			text_sprite.x = horz_margin;
			text_sprite.y = vert_margin;

			var box_width:int = text_sprite.width + horz_margin * 2;
			var box_height:int = text_sprite.height + vert_margin * 2;

			super (xml, box_width, box_height, move, drop);

			box.graphics.lineStyle (1, border_color, border_alpha, true);
			box.graphics.beginFill (background_color, background_alpha);
			box.graphics.drawRoundRect (0, 0, box_width, box_height, corner_radius, corner_radius);
			box.buttonMode = true;
			box.addChild (text_sprite);
			this.addEventListener (MouseEvent.MOUSE_DOWN, OnMouseDown);
			addChild (box);
		}
		public function ClearDroppings ():void
		{
			if (drop_window){
				var i:int = drop_window.drop_array.indexOf (this);
				if (i >= 0) drop_window.drop_array.splice (i,1);
				drop_window = null;}
		}
		public function ShowInWindow ():void
		{
			desired_x = drop_window.x + drop_window.dragtion_xpos;
			desired_y = drop_window.y + drop_window.dragtion_ypos;
			drop_window.dragtion_ypos += this.height + box_gap;
		}
	}
}
