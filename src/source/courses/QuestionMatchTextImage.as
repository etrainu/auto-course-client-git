package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import source.general.*;

	public class QuestionMatchTextImage extends QuestionMatch
	{
		public static var image_width:int = 160;
		public static var image_height:int = 90;

		public function QuestionMatchTextImage (ques_xml:XML):void
		{
			super (ques_xml);

			var area_width:int = options_width ? options_width : Course.page_width;
			left_width = image_width;
			right_width = area_width - left_width - 6;
			box_height = image_height;

			var option_xml_list:XMLList = ques_xml.option;
			for each (var option_xml:XML in option_xml_list){
				var option_text:String = option_xml.text;
				var image_url:String = Course.ResolveContentPath ("images", option_xml.image.@url);
				var option_left:OptionDrag = new OptionMatchImage (option_xml, left_width, box_height, image_url, DraggingLeft, Activity);
				var option_right:OptionDrag = new OptionMatchText (option_xml, right_width, box_height, option_text, DraggingRight, Activity);
				options_left.push (option_left);
				options_right.push (option_right);}

			ShuffleRightColumn ();
		}
	}
}