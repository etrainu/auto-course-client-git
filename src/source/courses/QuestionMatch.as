package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	
	import source.general.*;

	public class QuestionMatch extends Question
	{
		public var options_left:Array = [];
		public var options_right:Array = [];
		public var answer_count:int;
		public var box_height:int = 38;
		public var left_width:int;
		public var right_width:int;
		public var box_ypos:int

		public function QuestionMatch (xml:XML):void
		{
			super (xml);
		}
		public function ShuffleRightColumn ():void
		{
			var count:int = options_right.length;
			for (var i:int = 0; i<count-2; i++){
				var j:int = i + 1 + Math.floor (Math.random() * (count - i - 1));
				var t:* = options_right[i];
				options_right[i] = options_right[j];
				options_right[j] = t;}
		}		
		public override function RenderQuestion ():Sprite
		{
			var question_sprite:Sprite = new Sprite;
			var head_sprite:Sprite = GetQuestionHead();
			question_sprite.addChild (head_sprite);

			var xpos:int = 0;
			var ypos:int = head_sprite.height + text_gap;
			if (options_xpos != 0) xpos = options_xpos;
			if (options_ypos != 0) ypos = options_ypos;
			box_ypos = ypos;

			for (var i:int = 0; i < options_left.length; i++)
			{
				var option1:OptionDrag = options_left[i];
				var option2:OptionDrag = options_right[i];
				option1.ShowAt (xpos, ypos);
				option2.ShowAt (xpos + Course.page_width - right_width, ypos);
				ypos += box_height + Option.box_gap;
				question_sprite.addChild (option1);
				question_sprite.addChild (option2);
			}
			return question_sprite;
		}
		public function DraggingLeft (option:Option):void
		{
			options_left = UpdateHandler (options_left);
		}
		public function DraggingRight (option:Option):void
		{
			options_right = UpdateHandler (options_right);
		}
		public function UpdateHandler (_array:Array):Array
		{
			var array:Array = _array.sortOn ("y", Array.NUMERIC);

			var ypos:int = box_ypos;
			for (var i:int=0; i<array.length; i++){
				var option:OptionDrag = array[i];
				option.desired_y = ypos;
				ypos += box_height + Option.box_gap;}

			return array;
		}
		public override function OnEnterFrame (event:Event):void
		{
			for (var i:int = 0; i < options_left.length; i++){
				var option1:OptionDrag = options_left[i];
				var option2:OptionDrag = options_right[i];
				option1.OnEnterFrame (event);
				option2.OnEnterFrame (event);}
		}
		public override function Cheat():void
		{
			for (var i:int = 0; i < options_left.length; i++){
				var option1:OptionDrag = options_left[i];
				for (var j:int = 0; j < options_right.length; j++){
					var option2:OptionDrag = options_right[j];
					if (option1.option_ord == option2.option_ord){
						option2.desired_y = option1.y;}}}
			super.Cheat();
		}
		public override function IsCorrect ():Boolean
		{
			options_left = options_left.sortOn ("desired_y", Array.NUMERIC);
			options_right = options_right.sortOn ("desired_y", Array.NUMERIC);

			var is_correct:Boolean = true;
			for (var i:int = 0; i < options_left.length; i++){
				var option1:OptionDrag = options_left[i];
				var option2:OptionDrag = options_right[i];
				if (option1.option_ord != option2.option_ord){
					is_correct = false;}}
			return is_correct;
		}
		public override function StudentResponse():String
		{
			var response:String = "";
			for (var i:int = 0; i < options_left.length; i++){
				if (response.length) response += ",";
				response += options_left[i].option_ord + "." + options_right[i].option_ord;}
			return response;
		}
		public override function CorrectResponse():String
		{
			var response:String = "";
			for (var i:int = 0; i < options_left.length; i++){
				if (response.length) response += ",";
				response += options_left[i].option_ord + "." + options_left[i].option_ord;}
			return response;
		}
		public override function Reset():void
		{
			// should not need resetting
		}
	}
}
