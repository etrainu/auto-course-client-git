package source.courses
{
	public class CourseDoc
	{
		public static function Build ():void
		{
			var editor:AutoCourse = AutoCourse.editor;
			var style:String = "<basefont face=\"Calibri\">";
			style += "<style>";
			style += "p { font-size:12px; font-family:Calibri; margin:4px 0 8px }";
			style += ".h1 { font-size:22px; font-family:Calibri; color:#405080; margin:0 0 6px 8px }";
			style += ".h2 { font-size:20px; font-family:Calibri; color:#304070; margin:6px 0 6px 8px }";
			style += ".h3 { font-size:18px; font-family:Calibri; color:#203060; margin:6px 0 6px 8px }";
			style += "table { width:100%; }";
			style += "td { font-size:12px; font-family:Calibri; padding:0; margin:0 }";
			style += ".question { font-size:16px;  font-family:Calibri; background-color:#405060; color:white; padding:4px }";
			style += ".options { font-size:14px;  font-family:Calibri; background-color:#708090; color:white; padding:4px }";
			style += ".column { border: 1px solid #AAA; font-size:12px;  font-family:Calibri; background-color:#E0E4E8; padding:4px }";
			style += ".border { border: 1px solid #BBB; padding:4px }";
			style += ".help { border: 1px solid #CCC; padding:4px; color:#888 }";
			style += "</style>"
			var out:String = style;
			out += "<table><tr><td style=\"padding:4px\">";
			var stage_count:int = 1;
			var stage_xml_list:XMLList = editor.course_xml.stage;
			for each (var stage_xml:XML in stage_xml_list){
				if (stage_xml_list.length() > 1){
					out += "<p class=\"h2\">Stage " + stage_count++ + ": " + stage_xml.@name + "</p>";}
				var ass_count:int = 1;
				var ass_xml_list:XMLList = stage_xml.assessment;
				for each (var ass_xml:XML in ass_xml_list){
					if (ass_xml_list.length() > 1){
						out += "<p class=\"h3\">Assessment " + ass_count++ + ": " + ass_xml.@name + "</p>";}
					else out += "<p class=\"h3\">Assessment questions</p>";
					var question_xml_list:XMLList = ass_xml.question;
					for each (var question_xml:XML in question_xml_list){
						var qtype:String = question_xml.@type;
						var question_type:String = editor.question_dropdown_list.source.(@type == qtype).@label;
						
						out += "<table class=\"border\"><tr><td><table><tr>";
						out += "<td class=\"question\" width=\"75%\">" + question_xml.@name + "</td>";
						out += "<td align=\"center\" width=\"25%\" class=\"border\">" + question_type + "</td>";
						out += "</tr><tr><td colspan=\"2\">";
						out += "<p>" + question_xml.text + "</p>";
						out += "</td></tr><tr>"
						out += "<td class=\"options\">Options</td>";
						var option_xml_list:XMLList = question_xml.option;
						
						if (qtype == "multiple choice"){
							var option_count:int = question_xml.option.(@answer == "true").length();
							out += "<td align=\"center\" class=\"help\">Please tick " + option_count + "</td>";
							out += "</tr><tr><td colspan=\"2\"><table style=\"border-collapse: collapse\">";
							for each (var option_xml:XML in option_xml_list){
								var option_mark:String = (option_xml.@answer == "true") ? "X" : "";
								out += "<tr><td class=\"border\">" + String (option_xml.text) + "</td>";
								out += "<td class=\"border\" width=\"40\" align=\"center\">" + option_mark + "</td></tr>"}}
							
						else if (qtype == "match text text"){
							out += "<td align=\"center\" class=\"help\">Drag up and down</td>";
							out += "</tr><tr><td colspan=\"2\"><table style=\"border-collapse: collapse\">";
							for each (option_xml in option_xml_list){
								out += "<tr><td class=\"border\">" + String (option_xml.left) + "</td>";
								out += "<td class=\"border\">" + String (option_xml.right) + "</td></tr>"}}
							
						else if (qtype == "match text image"){
							out += "<td align=\"center\" class=\"help\">Drag up and down</td>";
							out += "</tr><tr><td colspan=\"2\"><table style=\"border-collapse: collapse\">";
							for each (option_xml in option_xml_list){
								out += "<tr><td class=\"border\">" + String (option_xml.image.@url) + "</td>";
								out += "<td class=\"border\">" + String (option_xml.text) + "</td></tr>"}}
							
						else if (qtype == "drag drop text"){
							out += "<td align=\"center\" class=\"help\">Drag text into column</td>";
							out += "</tr><tr><td colspan=\"2\"><table style=\"border-collapse: collapse\">";
							var table_array:Array = [];
							var drop_xml_list:XMLList = option_xml_list.(@type == "drop");
							var drag_xml_list:XMLList = option_xml_list.(@type == "drag");
							var row_count:int = 0;
							var col_count:int = drop_xml_list.length();
							for each (var drop_xml:XML in drop_xml_list){
								var col_array:Array = [];
								col_array.push (drop_xml.text);
								table_array.push (col_array);
								for each (var drag_xml:XML in drag_xml_list){
									if (drop_xml.@group == drag_xml.@group){
										col_array.push (drag_xml.text);
										row_count = Math.max (row_count, col_array.length);}}}
							for (var row:int=0; row<row_count; row++){
								out += "<tr>";
								for (var col:int=0; col<col_count; col++){
									col_array = table_array[col];
									if (row == 0) out += "<td class=\"column\">";
									else out += "<td class=\"border\">";
									if (row < col_array.length){
										out += col_array[row];}
									out += "</td>";}
								out += "</tr>";}}
						
						out += "</table></td></tr></table></td></tr></table>";
						out += "<p style=\"font-size:10px; margin:0; padding:0\">&nbsp;</p>";}}}
			
			out += "</td></tr></table>";
			editor.course_document.htmlText = out;
		}
	}
}