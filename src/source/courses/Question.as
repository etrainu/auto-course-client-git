package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import source.general.*;

	public class Question
	{
		public var question_xml:XML;
		public var question_ord:String;
		public var question_number:String;
		public var question_text:String;
		public var sound_url:String;
		public var activity_handler:Function;

		// question page options
		public static var title_size:int = 24;
		public static var title_color:uint = 0x002040;
		public static var title_gap:int = 20;
		public static var text_size:int = 13;
		public static var text_color:uint = 0x000000;
		public static var text_gap:int = 20;
		public static var help_color:uint = 0x707070;
		public static var help_size:int = 11;
		// change options position
		public static var options_xpos:int = 0;
		public static var options_ypos:int = 0;
		public static var options_width:int = 0;
		public static var question_width:int = 0;
		// results page options
		public static var result_size:int = 14;
		public static var result_color:uint = 0x000000;
		public static var result_gap:int = 10;
		public static var result_tick_class:Class = ResultTick;
		public static var result_cross_class:Class = ResultCross;

		public function Question (ques_xml:XML):void
		{
			question_xml = ques_xml;
			question_ord = ques_xml.@ord;
			question_number = String (ques_xml.@ord).replace (/[^0-9]/g, "");
			question_text = ques_xml.text[0].toString();
			if (ques_xml.sound.length())
				sound_url = ques_xml.sound.@url;
		}
		public function IsCorrect ():Boolean
		{
			return false;
		}
		public function GetResultSprite ():Sprite
		{
			var sprite:Sprite = new Sprite;
			var title:String = "Question " + question_number;
			var text_sprite:Sprite = GeneralText.MakeText (title, result_size, result_color, 0, 0);
			var is_correct:Boolean = IsCorrect();
			var tick_sprite:DisplayObject;
			if (is_correct) { tick_sprite = new result_tick_class;}
			else tick_sprite = new result_cross_class;
			tick_sprite.x = text_sprite.width + 12;
			sprite.addChild (text_sprite);
			sprite.addChild (tick_sprite);
			return sprite;
		}
		public function RenderQuestion ():Sprite
		{
			return GetQuestionHead();
		}
		public function GetQuestionHead ():Sprite
		{
			var sprite:Sprite = new Sprite;
			var title:String = "Question " + question_number;
			var text_width:int = question_width ? question_width : Course.page_width;
			var title_sprite:Sprite = GeneralText.MakeText (title, title_size, title_color, 0, 0);
			var text_sprite:Sprite = GeneralText.MakeText (question_text, text_size, text_color, 0, title_sprite.height + title_gap, text_width);
			sprite.addChild (title_sprite);
			sprite.addChild (text_sprite);
			return sprite;
		}
		public function Activity (option:Option):void
		{
			if (activity_handler != null){
				activity_handler (this, option);}
		}
		public function Cheat():void { Activity (null); }
		public function Reset():void {}
		public function OnEnterFrame (event:Event):void {}
		public function StudentResponse():String { return "";}
		public function CorrectResponse():String { return "";}
	}
}
