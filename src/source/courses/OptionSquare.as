package source.courses
{
	import flash.display.*;
	
	public class OptionSquare extends Sprite
	{
		public static var color:uint = Option.text_color;
		
		public function OptionSquare ()
		{
			var s:Sprite = new Sprite;
			var g:Graphics = s.graphics;
			g.lineStyle (1, color);
			g.beginFill (0, 0);
			g.drawRect (6, 8, 10, 10);
			addChild (s);
		}
	}
}
