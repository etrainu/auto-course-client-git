package source.courses
{
	import flash.display.*;
	import flash.text.*;
	import source.custom.*;
	import source.general.*;
	
	public class Option extends Sprite
	{
		public var option_xml:XML;
		public var option_ord:String;
		public var option_group:String;
		// text appearance
		public static var text_color:uint = 0x303030;
		public static var text_size:int = 12;
		public static var text_gap:int = 10;
		// drag and drop boxes
		public static var border_color:uint = 0x000000;
		public static var border_alpha:Number = 0.2;
		public static var background_color:uint = 0x000000;
		public static var background_alpha:Number = 0.02;
		public static var horz_margin:Number = 6;
		public static var vert_margin:Number = 3;
		public static var corner_radius:Number = 8;
		public static var box_gap:Number = 6;

		public function Option (xml:XML)
		{
			option_xml = xml;
			option_ord = xml.@ord;
			option_group = xml.@group;
		}
	}
}
