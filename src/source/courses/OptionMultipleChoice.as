﻿package source.courses
{
	import flash.display.*;
	import flash.text.*;
	import source.custom.*;
	import source.general.*;

	public class OptionMultipleChoice extends Option
	{
		private var option_checked:Boolean = false;
		private var correct_answer:Boolean = false;
		private var image_checked:DisplayObject;
		private var image_unchecked:DisplayObject;

		public static var option_checked_class:Class = OptionTick;
		public static var option_unchecked_class:Class = OptionSquare;

		public function OptionMultipleChoice (option_xml:XML)
		{
			super (option_xml);

			image_checked = new option_checked_class;
			image_unchecked = new option_unchecked_class;
			addChild (image_checked);
			addChild (image_unchecked);
			image_checked.visible = false;
			var text:String = String (option_xml.text);
			correct_answer = (String (option_xml.@answer) == "true") ? true : false;
			var text_sprite:CustomSprite = GeneralText.MakeText (text, text_size, text_color, 28, 4, Course.page_width);
			addChild (text_sprite);

			this.useHandCursor = true;
			this.buttonMode = true;
		}
		public function IsChecked():Boolean
		{
			return option_checked;
		}
		public function Toggle():void
		{
			if (option_checked) Uncheck();
			else Check();
		}
		public function Uncheck():void
		{
			option_checked = false;
			image_checked.visible = false;
			image_unchecked.visible = true;
		}
		public function Check():void
		{
			option_checked = true;
			image_checked.visible = true;
			image_unchecked.visible = false;
		}
		public function IsCorrect():Boolean
		{
			return option_checked == correct_answer;
		}
		public function IsCorrectAnswer():Boolean
		{
			return correct_answer;
		}
		public function Cheat ():void
		{
			if (correct_answer) Check();
			else Uncheck();
		}
	}
}

