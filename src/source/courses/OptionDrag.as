package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	
	import source.custom.*;
	import source.general.*;

	public class OptionDrag extends Option
	{
		public var box_width:int;
		public var box_height:int;
		public var desired_x:int;
		public var desired_y:int;
		public var dragging:Boolean = false;
		public var dropping:Boolean = false;
		public var move_handler:Function;
		public var drop_handler:Function;

		public function OptionDrag (xml:XML, _width:int, _height:int, move:Function, drop:Function)
		{
			super (xml);
			box_width = _width;
			box_height = _height;
			move_handler = move;
			drop_handler = drop;
		}
		public function ShowAt (_x:int, _y:int):void
		{
			desired_x = x = _x;
			desired_y = y = _y;
		}
		public function OnMouseDown (event:MouseEvent):void
		{
			stage.addEventListener (MouseEvent.MOUSE_UP, OnMouseUp);
			stage.addEventListener (MouseEvent.MOUSE_MOVE, OnMouseMove);
			this.startDrag ();
			dragging = true;
		}
		public function OnMouseUp (event:MouseEvent):void
		{
			this.stopDrag ();
			stage.removeEventListener (MouseEvent.MOUSE_UP, OnMouseUp);
			dragging = false;
			dropping = true;
			if (drop_handler != null)
				drop_handler (this);
		}
		public function OnMouseMove (event:MouseEvent):void
		{
			if (move_handler != null)
				move_handler (this);
		}
		public function OnEnterFrame (event:Event):void
		{
			if (!dragging){
				x += (desired_x - x) / 2;
				y += (desired_y - y) / 2;
				if (Math.abs (desired_x - x) < 1 && Math.abs (desired_y - y) < 1){
					x = desired_x;
					y = desired_y;}}
		}
	}
}
