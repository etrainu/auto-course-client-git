package source.courses
{
	import flash.events.Event;
	import flash.external.*;

	public class Scorm
	{
		public static var enable:Boolean = true;
		public static var logging:Boolean = false;
		public static var objectives:Array = [];
		public static var interactions:Array = [];
		public static var started:Boolean = false;
		public static var log_function:Function;

		public static function CourseStart (course_xml:XML):void
		{
			if (!started){
				objectives = [];
				interactions = [];
				var ass_xml_list:XMLList = course_xml..assessment.(@mode == "scorm");
				for each (var ass_xml:XML in ass_xml_list){
					GetObjectiveIndex (GetUniqueID (ass_xml));}
				LMSSetValue ("cmi.core.lesson_status", "incomplete");
				started = true;}
		}
		public static function AssessmentStart (ass_xml:XML):void
		{
			var auid:String = GetUniqueID (ass_xml);
			var index:int = GetObjectiveIndex (auid);
			var base:String = "cmi.objectives." + index;
			LMSSetValue (base + ".id", auid);
			LMSSetValue (base + ".score.min", "0");
			LMSSetValue (base + ".score.max", "100");
			LMSSetValue (base + ".status", "incomplete");
			objectives[index].status = "started";
		}
		public static function QuestionAnswered (ques_xml:XML, correct_response:String, student_response:String, latency:String, result:Boolean):void
		{
			var ass_xml:XML = ques_xml.parent();
			var quid:String = GetUniqueID (ques_xml);
			var auid:String = GetUniqueID (ass_xml);
			var index:int = GetInteractionIndex (quid);
			var base:String = "cmi.interactions." + index;
			LMSSetValue (base + ".id", quid);
			LMSSetValue (base + ".objectives.0.id ", auid);
			LMSSetValue (base + ".type", TranslateQuestionType (ques_xml.@type));
			LMSSetValue (base + ".correct_responses.0.pattern", correct_response);
			LMSSetValue (base + ".student_response", student_response);
			LMSSetValue (base + ".result", result ? "correct" : "wrong");
			LMSSetValue (base + ".latency", latency);
		}
		public static function AssessmentPassed (ass_xml:XML, score:Number):void
		{
			var auid:String = GetUniqueID (ass_xml);
			var index:int = GetObjectiveIndex (auid);
			var base:String = "cmi.objectives." + index;
			LMSSetValue (base + ".score.raw", String (score));
			LMSSetValue (base + ".status", "passed");
			objectives[index].status = "passed";
			CheckCourseCompleted ();
			LMSCommit();
		}
		public static function AssessmentFailed (ass_xml:XML, score:Number):void
		{
			var auid:String = GetUniqueID (ass_xml);
			var index:int = GetObjectiveIndex (auid);
			var base:String = "cmi.objectives." + index;
			LMSSetValue (base + ".score.raw", String (score));
			LMSSetValue (base + ".status", "failed");
			objectives[index].status = "failed";
			LMSCommit();
		}
		public static function CheckCourseCompleted ():void
		{
			for each (var object:* in objectives){
				if (object.status != "passed"){
					return;}}
			CourseCompleted();
		}
		public static function CourseCompleted ():void
		{
			LMSSetValue ("cmi.core.lesson_status", "completed");
		}
		public static function LMSSetValue (property:String, value:String):void
		{
			if (!enable) return;
			try {
				if (ExternalInterface.available){
					ExternalInterface.call ("LMSSetValue", property, value);}}
			catch (e:Event){}
			Log ("LMSSetValue (\"" + property + "\", \"" + value + "\")");
		}
		public static function LMSCommit ():void
		{
			if (!enable) return;
			try {
				if (enable && ExternalInterface.available){
					ExternalInterface.call ("LMSCommit", "");}}
			catch (e:Event){}
			Log ("LMSCommit()");
		}
		public static function GetObjectiveIndex (id:String):int
		{
			for (var index:int=0; index<objectives.length; index++){
				if (objectives[index].id == id) break;}
			if (index == objectives.length){
				objectives.push ({id:id, status:""});}
			return index;
		}
		public static function GetInteractionIndex (id:String):int
		{
			var index:int = interactions.indexOf (id);
			if (index == -1){ index = interactions.length; interactions.push (id);}
			return index;
		}
		public static function GetUniqueID (xml:XML):String
		{
			var uid:String = "";
			var xml_parent:XML = xml.parent();
			if (xml_parent){
				uid = GetUniqueID (xml_parent);}
			var ord_list:XMLList = xml.@ord;
			if (ord_list.length()){
				if (uid.length) uid += "_";
				uid += String (ord_list[0]);}
			return uid;
		}
		public static function TranslateQuestionType (rob_type:String):String
		{
			var scorm_type:String = "other";
			var rob_types:Array = ["multiple choice", "match text text", "match text image", "drag drop text"];
			var scorm_types:Array = ["choice", "matching", "matching", "dragdrop"];
			var i:int = rob_types.indexOf (rob_type);
			if (i != -1){ scorm_type = scorm_types[i];}
			return scorm_type;
		}
		public static function Log (message:String):void
		{
			if (log_function != null) log_function (message);
			if (logging) trace (message);
		}
	}
}

