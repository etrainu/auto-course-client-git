package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import source.custom.*;
	import source.general.*;

	public class Course
	{
		public static var canvas:Sprite;
		public static var page_x:int = 0;
		public static var page_y:int = 0;
		public static var page_width:int = 500;
		public static var page_height:int = 400;
		public static var button_back:*;
		public static var button_forward:*;
		public static var button_cheat:*;
		public static var button_events:Boolean = true;
		public static var cheat_mode:Boolean = false;
		public static var current_slide:Slide;
		public static var content_folder:String;
		private static var course_xml:XML;
		public static var title_function:Function;

		public static function set xml (xml:XML):void
		{
			course_xml = EvolveXML (xml);
			Scorm.CourseStart (xml);
		}
		public static function Load (url:String):void
		{
			GeneralLoader.LoadURL (url, LoadComplete)
		}
		public static function LoadComplete (loader:CustomURLLoader):void
		{
			course_xml = XML (loader.data);
			content_folder = course_xml.info.content.@folder;
		}
		public static function ResolveContentPath (subfolder:String, filename:String):String
		{
			var path:String = "";
			if (content_folder) path = content_folder + "/";
			if (subfolder) path += subfolder + "/";
			path = (path + filename).replace (/\/+/g, "/");
			return path;
		}
		public static function PlayCourse ():void
		{
			PlayStage ();
		}
		public static function PlayStage (id:* = 0):void
		{
			var stage_xml:XML = null;
			if (id is int) stage_xml = course_xml.stage[id];
			else if (id is String) stage_xml = course_xml.stage.(@name == id)[0];
			if (stage_xml) PlayStageXML (stage_xml);
		}
		public static function PlayStageXML (stage_xml:XML):void
		{
			var slide_xml_list:XMLList = stage_xml.children();
			if (slide_xml_list.length()){
				PlaySlideXML (slide_xml_list[0]);}
		}
		public static function PlaySlide (id:* = 0):void // both
		{
			var slide_xml:XML = null;
			if (id is int) slide_xml = course_xml.stage.children()[id];
			else if (id is String) slide_xml = course_xml.stage.children().(@name == id)[0];
			PlaySlideXML (slide_xml);
		}
		public static function PlayAssessment (id:* = 0):void
		{
			var ass_xml:XML = null;
			if (id is int) ass_xml = course_xml.stage.assessment[id];
			else if (id is String) ass_xml = course_xml.stage.assessment.(@name == id)[0];
			PlaySlideXML (ass_xml);
		}
		public static function PlaySlideXML (slide_xml:XML):void
		{
			if (title_function != null) title_function (slide_xml);
			if (slide_xml.name() == "assessment") NewAssessment (slide_xml).AskQuestions();
			else SetCurrentSlide (new Slide (slide_xml)).ShowSlideXML (slide_xml);
		}
		public static function PlayElementXML (element_xml:XML):void
		{
			SetCurrentSlide (new Slide (element_xml)).ShowElementXML (element_xml);
		}
		public static function PlayQuestionXML (question_xml:XML):void
		{
			Assessment (SetCurrentSlide (new Assessment (question_xml.parent(), true))).JumpToQuestion (question_xml.@ord);
		}
		public static function NewAssessment (ass_xml:XML):Assessment
		{
			return Assessment (SetCurrentSlide (new Assessment (ass_xml)));
		}
		public static function SetCurrentSlide (slide:Slide):Slide
		{
			if (current_slide){
				if (canvas.contains (current_slide)){
					canvas.removeChild (current_slide);}}
			canvas.addChild (slide);
			current_slide = slide;
			slide.x = page_x;
			slide.y = page_y;
			return slide;
		}
		public static function PlayNextSlide (direction:int=1):void
		{
			if (current_slide){
				var current_xml:XML = current_slide.slide_xml;
				var current_index:int = current_xml.childIndex();
				var next_index:int = current_index + direction;
				var parent_xml:XML = current_xml.parent();
				var children:XMLList = parent_xml.children();
				var slide_count:int = children.length();
				if (next_index < slide_count){
					var next_slide_xml:XML = children[next_index];
					PlaySlideXML (next_slide_xml);}
				else if (next_index == slide_count){
					/* next stage */ }
			}
		}
		public static function Close ():void
		{
			if (current_slide){
				if (canvas.contains (current_slide)){
					canvas.removeChild (current_slide);}
				current_slide = null;}
		}
		public static function CheatQuestion ():void
		{
			if (GetAssessment()){
				GetAssessment().CheatQuestion();}
		}
		public static function ClickBack ():void
		{
			if (current_slide){
				if (current_slide.NavigateBack (null)){
					PlayNextSlide (-1);}}
		}
		public static function ClickForward ():void
		{
			if (current_slide){
				if (current_slide.NavigateForward (null)){
					PlayNextSlide (1);}}
		}
		public static function GetAssessment ():Assessment
		{
			if (current_slide && current_slide is Assessment)
				return Assessment (current_slide);
			return null;
		}
		public static function EvolveXML (cxml:XML):XML
		{
			// assessments v0.4
			var xml_list:XMLList = cxml.stage.assessment;
			for each (var xml:XML in xml_list){
				if (!xml.@mode.length()){
					xml.@mode = "scorm";}
				if (xml.sound.length()){
					delete xml.sound;}}
			// questions v0.3
			xml_list = cxml.stage.assessment.question.(@type == "choice");
			for each (xml in xml_list)
				xml.@type = "multiple choice"; 
			xml_list = cxml.stage.assessment.question.(@type == "match");
			for each (xml in xml_list)
				xml.@type = "match text text";
			return cxml;
		}
	}
}
