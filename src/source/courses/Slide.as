package source.courses
{
	import flash.display.*;
	import flash.events.*;
	
	import source.custom.*;
	import source.general.*;
	
	public class Slide extends Sprite
	{
		public var slide_xml:XML;
		public var html_control:*;
		
		public function Slide (xml:XML)
		{
			slide_xml = xml;
		}
		public function ShowSlideXML (xml:XML):void
		{
			var ypos:int = 0;
			var element_list:XMLList = xml.element;
			for each (var element_xml:XML in element_list){
				ypos += ShowElementXML (element_xml, ypos);}
		}
		public function ShowElementXML (element_xml:XML, ypos:int=0):int
		{
			var type:String = element_xml.@type;

			if (type == "text"){
				var text:String = unescape (element_xml.text);
				var text_sprite:Sprite = GeneralText.MakeText (text, 12, 0, 0, ypos, Course.page_width);
				return addChild (text_sprite).height;}

			if (type == "html"){
				//html_control.htmlText = "<head><style>body{font-size:10px}</style></head><body>" + total_text + "</body>";
			}

			return 0;
		}
		public function NavigateForward (e:Event=null):Boolean
		{
			return true;
		}
		public function NavigateBack (e:Event=null):Boolean
		{
			return true;
		}
	}
}
