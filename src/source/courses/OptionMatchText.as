package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	
	import source.custom.*;
	import source.general.*;
	
	public class OptionMatchText extends OptionDrag
	{
		public var text_sprite:CustomSprite;

		public function OptionMatchText (xml:XML, _width:int, _height:int, text:String, move:Function, drop:Function)
		{
			super (xml, _width, _height, move, drop);
			addChild (MakePanel (_width, _height, text));
			this.addEventListener (MouseEvent.MOUSE_DOWN, OnMouseDown);
		}
		public static function MakePanel (_width:int, _height:int, text:String):Sprite
		{
			var panel : Sprite = new Sprite;
			var text_width:int = _width ? _width - horz_margin * 2 : 0;
			var text_sprite:CustomSprite = GeneralText.MakeText (text, text_size, text_color, 0, 0, text_width);
			text_sprite.x = horz_margin;
			text_sprite.y = vert_margin;
			panel.graphics.lineStyle (1, border_color, border_alpha, true);
			panel.graphics.beginFill (background_color, background_alpha);
			var panel_width:int = text_sprite.width + 2 * horz_margin;
			var panel_height:int = _height ? _height : text_sprite.height + 2 * vert_margin;
			panel.graphics.drawRoundRect (0, 0, panel_width, panel_height, corner_radius, corner_radius);
			panel.buttonMode = true;
			panel.addChild (text_sprite);
			return panel
		}
	}
}
