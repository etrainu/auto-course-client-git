package source.browser.components
{
	import mx.controls.Text;
	import source.browser.Constants;

	public class SimpleText extends Text
	{
		public function SimpleText():void
		{
			super();
			setStyle("leading", 2);
			setStyle("paddingLeft", -2);
			setStyle("paddingRight", -3);
			setStyle("paddingTop", -3);
			setStyle("paddingBottom", -2);
		}
	}
}
