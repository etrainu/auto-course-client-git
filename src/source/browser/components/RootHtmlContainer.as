package source.browser.components
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import mx.containers.Box;
	import mx.core.UIComponent;
	import mx.controls.Alert;

	public class RootHtmlContainer extends Box
	{
		public function RootHtmlContainer():void
		{
			super();

//For testing
//setStyle("borderColor", "#000000");
//setStyle("borderStyle", "solid");

			//Default to width of parent
			percentWidth = 100;

			addEventListener(Event.ADDED_TO_STAGE, function(event:Event):void
				{
					var self:UIComponent = event.currentTarget as UIComponent;
					if (self.parent == null)
						return;

					if ( !isNaN(self.percentWidth) )
					{
						self.width = self.parent.width * self.percentWidth / 100;
						for (var i:int = 0; i < numChildren; i++)
						{
							var o:UIComponent = getChildAt(i) as UIComponent;
							if (o == null)
								continue;
							o.maxWidth = self.width;
						}
					}
				});

			setStyle("color", "#000000");
			setStyle("fontFamily", "Times");
			setStyle("fontSize", 16);
			setStyle("leading", 0);
			setStyle("paddingTop", 0);
		}

		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
		
			if ( isNaN(unscaledWidth) && isNaN(unscaledHeight) )
				return;

			if (numChildren > 0)
			{
				var d:DisplayObject = getChildAt(0);
				setActualSize(width, d.height);
//				height = d.height;
//Alert.show("d.height=" + d.height);
//Alert.show("d.measuredHeight=" + (d as UIComponent).measuredHeight);
//Alert.show("self.height=" + (d as UIComponent).measuredHeight);
			}

		}
	}
}
