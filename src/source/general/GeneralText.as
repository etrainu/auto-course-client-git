﻿package source.general
{
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	import flash.net.*;
	import flash.text.*;
	import flash.ui.*;
	
	import source.custom.*;

	public class GeneralText
	{
		public static var embed_fonts:Boolean = false;
		public static var default_font:String = "Arial";
		public static var default_bold:Boolean = false;
		public static var style_sheet:StyleSheet = new StyleSheet;

		public static function SetStyle (style:String):void
		{
			style_sheet = new StyleSheet;
			style_sheet.parseCSS (style);
		}
		public static function MakeText (text:String, size:Number=12, color:uint=0, xpos:Number=0, ypos:Number=0, text_width:Number=0, font_face:String=null) : CustomSprite
		{
			var sprite : CustomSprite = new CustomSprite;
			var text_format:TextFormat = new TextFormat;
			text_format.font = font_face ? font_face : default_font;
			text_format.bold = default_bold;
			text_format.size = Math.floor (size);
			text_format.color = color;

			var text_field:TextField = new TextField;
			if (text_width){
				text_field.width = text_width;
				text_field.multiline= true;
				text_field.wordWrap = true;}

			text_field.selectable = false;
			text_field.mouseEnabled = false;
			text_field.embedFonts = embed_fonts;
			text_field.defaultTextFormat = text_format;
			if (style_sheet) text_field.styleSheet = style_sheet;
			text_field.condenseWhite = true;
			text_field.antiAliasType = AntiAliasType.ADVANCED;
			text_field.autoSize = TextFieldAutoSize.LEFT;
			text_field.htmlText = text;
			sprite.field = text_field;
			sprite.addChild (text_field);
			sprite.x = xpos;
			sprite.y = ypos;
			return sprite;
		}
		public static function MakeInput (text:String, size:Number, xpos:Number, ypos:Number, color:uint, text_width:Number, text_height:Number, font_face:String=null) : CustomSprite
		{
			var sprite : CustomSprite = new CustomSprite;
			var text_format:TextFormat = new TextFormat;
			text_format.font = font_face ? font_face : default_font;
			text_format.size = Math.floor (size);
			text_format.color = color;

			var text_field:TextField = new TextField;
			text_field.width = text_width;
			text_field.height = text_height;
			text_field.border = true;
			text_field.borderColor = 0xB0B0B0;
			text_field.embedFonts = embed_fonts;
			text_field.defaultTextFormat = text_format;
			text_field.antiAliasType = AntiAliasType.ADVANCED;
			text_field.type = TextFieldType.INPUT;
			text_field.text = text;
			sprite.addChild (text_field);
			sprite.x = xpos;
			sprite.y = ypos;
			return sprite;
		}
		public static function MakeButton (text:String, size:int=12, xpos:int=0, ypos:int=0, _width:int=0, line_color:uint=0x000000, back_color:uint=0x808080, back_alpha:Number=0.1) : Sprite
		{
			var horz_margin:Number = 12;
			var vert_margin:Number = 2;
			var round_radius:Number = 8;
			var box : Sprite = new Sprite;
			var text_field:Sprite = MakeText (text, size, line_color, 0, 0);
			var button_width:int = (_width) ? _width : text_field.width; 
			text_field.x = horz_margin+ (button_width - text_field.width) / 2;
			text_field.y = vert_margin;
			box.graphics.lineStyle (1, line_color, 0.4, true);
			box.graphics.beginFill (back_color, back_alpha);
			box.graphics.drawRoundRect (0, 0, button_width + horz_margin*2, text_field.height + vert_margin*2, round_radius, round_radius);
			box.buttonMode = true;
			box.x = xpos;
			box.y = ypos;
			box.addChild (text_field);
			return box;
		}
	}
}
