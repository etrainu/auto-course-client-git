package source.general
{
	import flash.display.Sprite;
	/*
		Spline class uses interpolation to determine any point
		on a line represented by an array of points.
		Allows for two types of curve:
			b-spline (b) does not pass through the points (smooth)
			cardinal spline (c) always passes though the points (accurate)
		The bb option assumes that the end points do not need to be duplicated
	*/
	public class GeneralSpline
	{
		public static function GetValue (array:Array, ratio:Number, easing:String="none", type:String="a") : Number
		{
			ratio = GeneralEasing.GetFunction (easing) (ratio);
			if (type == "b") return GetBSpline (array, ratio, true);
			if (type == "bb") return GetBSpline (array, ratio, false);
			if (type == "c") return GetCardinal (array, ratio);
			return GetLinear (array, ratio);
		}
		public static function GetLinear (array:Array, ratio:Number):Number
		{
			var i:int = Math.floor (ratio * (array.length - 1));
			if (i >= array.length - 1) return array[i];
			var t:Number = (ratio * (array.length - 1)) % 1;
			var v1:Number = array[i];
			var v2:Number = array[i+1];
			var u:Number = 1 - t;
			return v1 * u + v2 * t;
		}
		public static function GetCardinal (array:Array, ratio:Number):Number
		{
			var len:int = array.length - 1;
			var i:int = Math.floor (ratio * len);
			var t:Number = (ratio * len) % 1;
			if (i == len)
			{
				i = len - 1;
				t = 1.0;
			}

			if (i == 0){
				var p1 : Number = array[i];
				var p2 : Number = array[i];}
			else {
				var p1 : Number = array[i-1];
				var p2 : Number = array[i];}

			if (i >= array.length - 2){
				var p3 : Number = array[i+1];
				var p4 : Number = array[i+1];}
			else {
				var p3 : Number = array[i+1];
				var p4 : Number = array[i+2];}

			return SolveCatmullRom (p1, p2, p3, p4, t);			
		}
		public static function SolveCatmullRom (p1:Number, p2:Number, p3:Number, p4:Number, t:Number) : Number
		{
    		var t2 : Number = t * t;
    		var t3 : Number = t2 * t;
    		var b1 : Number = 0.5 * (  -t3 + 2*t2 - t);
    		var b2 : Number = 0.5 * ( 3*t3 - 5*t2 + 2);
    		var b3 : Number = 0.5 * (-3*t3 + 4*t2 + t);
    		var b4 : Number = 0.5 * (   t3 -   t2    );
    		return (p1*b1 + p2*b2 + p3*b3 + p4*b4);
		}
		public static function GetBSpline (array:Array, ratio:Number, pad:Boolean=true):Number
		{
			if (pad){
				array = array.slice();
				array.unshift (array[0]);
				array.push (array[array.length-1]);} 

			var len:int = array.length - 1;
			var i:int = Math.floor (ratio * len);
			var t:Number = (ratio * len) % 1;
			if (i == len)
			{
				i = len - 1;
				t = 1.0;
			}

			if (i == array.length-1 && t > 0.5)
				var debugging:Boolean = true;

			if (i == 0){
				var p1 : Number = array[i];
				var p2 : Number = array[i];}
			else {
				var p1 : Number = array[i-1];
				var p2 : Number = array[i];}

			if (i >= array.length - 2){
				var p3 : Number = array[i+1];
				var p4 : Number = array[i+1];}
			else {
				var p3 : Number = array[i+1];
				var p4 : Number = array[i+2];}

			return SolveBSpline (p1, p2, p3, p4, t);			
		}
		public static function SolveBSpline (p1:Number, p2:Number, p3:Number, p4:Number, t:Number) : Number
		{
		    var a:Array = new Array (4);

		    a[0] = (-p1 + 3 * p2 - 3 * p3 + p4) / 6.0;
		    a[1] = (3 * p1 - 6 * p2 + 3 * p3) / 6.0;
		    a[2] = (-3 * p1 + 3 * p3) / 6.0;
		    a[3] = (p1 + 4 * p2 + p3) / 6.0;

		    var value:Number = (a[2] + t * (a[1] + t * a[0]))*t + a[3];
		    return value;
		}
		/*
			Draws a graph demonstrating the two types of curve
			Not necessary other than for educational purposes
		*/
		public static function TestSpline () : Sprite
		{
			var sprite : Sprite = new Sprite
			var ys :Array = [ 0, 0, 50, 100, 0, -100, 200, -200, 200, 250, -300, -300, 150, 200, 150, 0, 0 ];

			sprite.graphics.lineStyle (1, 0x0000CC);
			sprite.graphics.moveTo (0, 0);
			var steps:Number = 300;
			var gwidth:Number = 1200;
			for (var i:int =0; i<steps; i++)
			{
				var xx:Number = i * (gwidth / steps);
				var yy:Number = GetValue (ys, xx / gwidth, "", "c");
				sprite.graphics.lineTo (xx, yy);
			}

			sprite.graphics.lineStyle (1, 0x55AA00);
			sprite.graphics.moveTo (0, 0);
			for (var i:int =0; i<steps; i++)
			{
				var xx:Number = i * (gwidth / steps);
				var yy:Number = GetValue (ys, xx / gwidth, "", "bb");
				sprite.graphics.lineTo (xx, yy);
			}

			sprite.graphics.lineStyle (1, 0xCC0000);
			sprite.graphics.moveTo (0, 0);
			for (var i:int =0; i<ys.length; i++)
			{
				var xx:Number = i * (gwidth / (ys.length-1));
				var yy:Number = ys[i];
				sprite.graphics.lineTo (xx, yy);
			}
			return sprite;
		}
	}
}
