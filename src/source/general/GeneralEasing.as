package source.general
{
	/*
	Typical easing functions
	*/
	public class GeneralEasing
	{
		public static function GetFunction (name : String) : Function
		{
			if (name == "in and out") return EaseInAndOut;
			else if (name == "in and out 2") return EaseInAndOut2;
			else if (name == "in") return EaseIn;
			else if (name == "out") return EaseOut;
			return None;
		}
		public static function None (w : Number, p : Number = 50) : Number
		{
			return w;
		}
		public static function EaseInAndOut (w : Number, p : Number = 50) : Number
		{
			return 0.5 - Math.cos (w * Math.PI) / 2;
		}
		public static function EaseInAndOut2 (w : Number, p : Number = 50) : Number
		{
			var a:Number = EaseInAndOut (w, p);
			return (a + w) / 2;
		}
		public static function EaseIn (v : Number, p : Number = 50) : Number
		{
			var a : Number;
			var b : Number;
			var c : Number;
			var d : Number;
			var u : Number;
			
			a = p / 100;
			if (a > 0){
				b = 1 / (a * a);
				c = 2 / a;
				d = c - 1;}
			
			if (a <= 0) return v;
			if (v < a) u = b * v * v;
			else u = (c * (v - a)) + 1;
			return u / d;
		}
		public static function EaseOut (w : Number, p : Number = 50) : Number
		{
			var a : Number;
			var b : Number;
			var c : Number;
			var d : Number;
			var u : Number;
			
			a = p / 100;
			if (a > 0){
				b = 1 / (a * a);
				c = 2 / a;
				d = c - 1;}
			
			if (a <= 0) return w;
			var v:Number = 1 - w;
			if (v < a) u = b * v * v;
			else u = (c * (v - a)) + 1;
			return 1 - u / d;
		}
	}
}
