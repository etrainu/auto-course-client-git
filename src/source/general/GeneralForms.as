package source.general
{
	import custom.*;
	import flash.display.*;
	import flash.events.*;
	import flash.filters.*;
	import flash.geom.*;

	public class GeneralForms
	{
		public static var ypos:Number = 0;
		public static var field_width:Number = 150;
		public static var input_width:Number = 260;
		public static var input_height:Number = 20;
		public static var margin:Number = 8;

		public static function Reset ():void
		{
			ypos = 0;
		}
		public static function DrawFormPair (name:String):CustomSprite
		{
			var lead:Number = 8;
			var pair:CustomSprite = new CustomSprite;
			var name_field:Sprite = GeneralText.MakeText (name, 14, 0, 0, 0x444444);
			var value_field:Sprite = GeneralText.MakeInput ("", 14, field_width+margin, 0, 0x444444, input_width, input_height);
			name_field.x = 4;
			pair.addChild (name_field);
			pair.addChild (value_field);
			pair.y = ypos;
			ypos += pair.height + lead;
			return pair;
		}
	}
}
