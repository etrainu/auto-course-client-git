package source.general
{
/*	
	A general purpose class for streaming sound
*/
	public class GeneralStreamingSound
	{	
		import flash.media.*;
		import flash.events.*;
		import flash.net.*;
		import mx.events.*;

		public var sound:Sound;
		public var channel:SoundChannel;
		public var volume_slider : *;
		public var position_slider : *;
		public var sound_url : String;
		public var is_streaming : Boolean = false;
		public var is_playing : Boolean = false;
		public var position : Number = 0;
		public var complete_handler : Function;

		public function GeneralStreamingSound (url:String, vol_slide:*=null, pos_slide:*=null)
		{
			sound_url = url;
			volume_slider = vol_slide;
			position_slider = pos_slide;
		}
		public function Play (volume:Number=1, handler:Function=null) : void
		{
			var req:URLRequest = new URLRequest (sound_url);
			var context:SoundLoaderContext = new SoundLoaderContext (1000, true);

			sound = new Sound;
			sound.addEventListener(ProgressEvent.PROGRESS, OnLoadProgress);
			sound.addEventListener(Event.COMPLETE, OnLoadComplete);
			sound.addEventListener(IOErrorEvent.IO_ERROR, OnIOError);
			sound.load (req, context);

			is_streaming = true;
			is_playing = true;

			channel = sound.play();
			//channel.soundTransform = new SoundTransform (volume);
			channel.addEventListener (Event.SOUND_COMPLETE, OnSoundComplete);

			if (handler != null) complete_handler = handler;
		}
		public function SetupControl (event:ProgressEvent):void
		{
			volume_slider.addEventListener (SliderEvent.CHANGE, OnVolumeChange);
			position_slider.addEventListener (SliderEvent.CHANGE, OnScrubberPos);
		}
		public function OnLoadProgress (event:ProgressEvent):void
		{
		    var loaded:Number = event.bytesLoaded / event.bytesTotal;
		    if (event.bytesLoaded == event.bytesTotal)
		    	is_streaming = false;
		}
		public function OnLoadComplete (event:Event):void
		{
			is_streaming = false;
		    //var localSound:Sound = event.target as Sound;
		    //localSound.play();
		}
		public function OnSoundComplete (event:Event):void
		{
			is_playing = false;
			if (complete_handler != null) complete_handler (this);
		}
		public function OnVolumeChange (event:Event):void
		{
			var volume:Number = event.currentTarget.value/100;
			if (channel) channel.soundTransform = new SoundTransform (volume);
		}
		public function OnScrubberPos (event:Event):void
		{

		}
		public function OnIOError (event:IOErrorEvent):void
		{

		}
		public function PlayPause () : void
		{
			if (!sound){
				Play ();
				return;}

			if (is_playing){
				is_playing = false;
				position = channel.position;
				channel.stop();
				channel = null;}
			else {
				channel = sound.play (position);
				channel.addEventListener (Event.SOUND_COMPLETE, OnSoundComplete);
				is_playing = true;}
		}
		public function SoundOnOff () : void
		{

		}
		public function Close () : void
		{
			if (sound){
				if (channel) try { channel.stop();} catch (e:*){};
				if (is_streaming) try { sound.close();} catch (e:*){};
				is_streaming = false;
				channel = null;
				sound = null;}
		}
	}
}
