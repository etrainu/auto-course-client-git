package source.general
{
	import flash.events.*;
	import flash.media.*;
	import flash.net.*;

	public class GeneralSound
	{	
		public static var sound:Sound;
		public static var channel:SoundChannel;
		//public static var content_folder:String = "";
		//public static var sounds_folder:String = "sounds/";
		public static var volume:Number = 1.0;

		public static function Play (url:String) : void
		{
			Stop ();

			//var url:String = content_folder;
			//if (url.length > 0 && url.charAt(url.length) != '/') url += '/';
			//url += sounds_folder + _url;

			var req:URLRequest = new URLRequest (url);
			var context:SoundLoaderContext = new SoundLoaderContext (1000, true);

			sound = new Sound;
			sound.addEventListener (IOErrorEvent.IO_ERROR, OnIOError);
			sound.load (req, context);
			channel = sound.play();
			if (channel)
				channel.soundTransform = new SoundTransform (volume);
		}
		public static function OnIOError (event:IOErrorEvent):void { /* dummy */ }

		public static function Stop () : void
		{
			if (sound){
				if (channel) try { channel.stop();} catch (e:*){};
				try { sound.close();} catch (e:*){};
				sound.removeEventListener (IOErrorEvent.IO_ERROR, OnIOError);
				channel = null;
				sound = null;}
		}
	}
}
