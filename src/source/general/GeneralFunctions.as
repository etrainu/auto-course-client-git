package source.general
{
	import flash.utils.*;
	
	public class GeneralFunctions
	{
		public static const to_radians : Number = Math.PI / 180;
		public static const to_degrees : Number = 180 / Math.PI;
		
		public static function GetHexColor (color : String) : uint
		{
			return uint ("0x" + color);
		}
		public static function ReverseHexColor (color : String) : uint
		{
			return uint ("0x" + color.substr(-2,2) + color.substr(-4,2) + color.substr(-6,2));
		}
		public static function GetHexAlpha (color : String) : Number
		{
			return Number (uint ("0x" + color.substr(0,2))) / 255;
		}
		public static function GetRGBColor (string : String) : uint
		{
			var array : Array = string.split (",");
			return 	uint (array[0]) * 0x10000 +
				uint (array[1]) * 0x100 +
				uint (array[2]);
		}
		public static function GetGradientColors (xml : XMLList) : Array
		{
			var string_array : Array = String (xml).split(",");
			var value_array : Array = [];
			for (var i:int=0; i<string_array.length; i++)
			{
				value_array.push (GetHexColor (string_array[i]));
			}
			return value_array;
		}
		public static function GetGradientRatios (xml : XMLList) : Array
		{
			var string_array : Array = String (xml).split(",");
			var value_array : Array = [];
			for (var i:int=0; i<string_array.length; i++)
			{
				var value:int = int (Number (string_array[i]) * 2.55 + 0.1);
				value_array.push (value);
			}
			return value_array;
		}
		public static function GetGradientAlphas (xml : XMLList) : Array
		{
			var string_array : Array = String (xml).split(",");
			var value_array : Array = [];
			for (var i:int=0; i<string_array.length; i++)
			{
				var value:Number = Number (string_array[i]) / 100;
				value_array.push (value);
			}
			return value_array;
		}
		public static function GetAlpha (xml : XMLList) : Number
		{
			return Number (String(xml)) / 100;
		}
		public static function GetRadians (angle : String) : Number
		{
			var value:Number = Number (angle) * Math.PI / 180;
			return value;
		}
		public static function GetCoordinate (xml : XMLList) : Array
		{
			var string_array : Array = String (xml).split(",");
			var value_array : Array = [];
			for (var i:int=0; i<string_array.length; i++)
			{
				var value:Number = Number (string_array[i]);
				value_array.push (value);
			}
			return value_array;
		}
		public static function XMLToPos (xml:XML):*
		{
			return {
				x:(Number (xml.@x)), y:(Number (xml.@y)), z:(Number (xml.@z)),
				rx:(Number (xml.@rx)), ry:(Number (xml.@ry)), rz:(Number (xml.@rz)) };
		}
		public static function CloneObject (source:Object):*
		{
			var myBA:ByteArray = new ByteArray;
			myBA.writeObject(source);
			myBA.position = 0;
			return(myBA.readObject());
		}
		public static function SimilateFilename (backup_filename:String, filename:String):String
		{
			var name:String = RemoveNumbers (RemoveExtension (filename));
			var backup_name:String = RemoveNumbers (RemoveExtension (backup_filename));
			if (name != backup_name) return filename;
			return backup_filename;
		}
		public static function RemoveExtension (filename:String):String
		{
			var e:int = filename.lastIndexOf(".");
			return (e == -1) ? filename : filename.slice (0, e);
		}
		public static function GetExtension (filename:String):String
		{
			var e:int = filename.lastIndexOf(".");
			return (e == -1) ? "" : filename.slice (e);
		}
		public static function RemoveNumbers (filename:String):String
		{
			for (var i:int=filename.length-1; i>=0; i--){
				var c:String = filename.charAt(i);
				if (c >= "0" && c <= "9")
					filename = filename.slice (0, -1);}
			return filename;
		}
		public static function IncrementFilename (filename:String, padding:int=3):String
		{
			var extension:String = GetExtension (filename);
			var name:String = RemoveExtension (filename);
			for (var i:int=name.length; i>0; i--){
				var c:String = name.charAt(i-1);
				if (c < "0" || c > "9") break;}
			var name_base:String = name.slice (0, i);
			var name_count:String = name.slice(i);
			var name_count_int:int = int (name_count);
			var new_count:String = String (name_count_int + 1);
			while (new_count.length < padding)
				new_count = "0" + new_count;
			return name_base + new_count + extension;
		}
		public static function GetDateString ():String
		{
			var date:Date = new Date;
			return "" + date.fullYear + "-" + date.month + "-" + date.date;
		}
	}
}
