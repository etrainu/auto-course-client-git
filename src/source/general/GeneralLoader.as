﻿package source.general
{
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	import flash.net.*;
	import flash.ui.*;

	import source.custom.*;

	public class GeneralLoader
	{
		public static function LoadURL (url:String, complete_fn:Function=null) : CustomURLLoader
		{
			var loader : CustomURLLoader = new CustomURLLoader;
			loader.complete = complete_fn;
			return LoadURLCore (loader, url);
		}
		public static function LoadURLCore (loader:CustomURLLoader, url:String) : CustomURLLoader
		{
			var request:URLRequest = new URLRequest (url + GeneralPreloader.cache_buster);
			loader.addEventListener (Event.COMPLETE, URLLoaded);
			loader.addEventListener (IOErrorEvent.IO_ERROR , GeneralPreloader.LoadError);
			try { loader.load(request);} catch (error:Error){ trace ("Unable to load requested URL: " + url);}
			return loader;
		}
		public static function URLLoaded (event:Event) : void
		{
			var loader : CustomURLLoader = CustomURLLoader (event.target);
			if (loader.complete != null) loader.complete (loader);
		}
		public static function LoadStream (url:String, complete_fn:Function=null) : CustomStream
		{
			var request:URLRequest = new URLRequest (url);
			var stream : CustomStream = new CustomStream();
			stream.addEventListener (Event.COMPLETE, StreamLoaded);
			stream.addEventListener (IOErrorEvent.IO_ERROR, GeneralPreloader.LoadError);
			stream.complete = complete_fn;
			try { stream.load(request);} catch (error:Error){ trace ("Unable to load requested URL");}
			return stream;
		}
		public static function StreamLoaded (event:Event) : void
		{
			var stream : CustomStream = CustomStream (event.target);
			if (stream.complete) stream.complete (stream);
		}
		public static function LoadImage (url:String, xpos:int=0, ypos:int=0, complete_fn:Function=null) : CustomLoader
		{
			var loader : CustomLoader = new CustomLoader;
			loader.x = xpos; loader.y = ypos;
			loader.complete = complete_fn;
			var request:URLRequest = new URLRequest (url);
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, ImageLoaded);
			loader.contentLoaderInfo.addEventListener (IOErrorEvent.IO_ERROR, GeneralPreloader.LoadError);
			try { loader.load (request);}
			catch (error:Error){ trace ("Unable to load requested URL");}
			return loader;
		}
		public static function ImageLoaded (event:Event) : void
		{
			var loader_info : LoaderInfo = LoaderInfo (event.target);
			var loader : CustomLoader = CustomLoader (loader_info.loader);
			loader.content.x = loader.xpos;
			loader.content.y = loader.ypos;
			//var bitmap:Bitmap = Bitmap(loader.content);
			//bitmap.smoothing = true;
		}
		public static function LoadSizedImage (url:String, new_width:Number, new_height:Number, complete_fn:Function=null) : CustomLoader
		{
			var loader : CustomLoader = new CustomLoader;
			loader.new_width = new_width;
			loader.new_height = new_height;
			loader.complete = complete_fn;
			var request:URLRequest = new URLRequest (url);
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, ResizeImage);
			loader.contentLoaderInfo.addEventListener (IOErrorEvent.IO_ERROR, GeneralPreloader.LoadError);
			try { loader.load (request);}
			catch (error:Error){ trace ("Unable to load requested URL");}
			return loader;
		}
		public static function ResizeImage (event:Event) : void
		{
			var loader_info : LoaderInfo = LoaderInfo (event.target);
			var loader : CustomLoader = CustomLoader (loader_info.loader);
			var image:DisplayObject = DisplayObject (loader.content);
			var aspect_ratio:Number = image.width / image.height;
			var new_aspect_ratio:Number = loader.new_width / loader.new_height;
			if (image is Bitmap) Bitmap (image).smoothing = true;
			if (aspect_ratio >= new_aspect_ratio){
				image.width = loader.new_width;
				image.height = loader.new_width / aspect_ratio;}
			else {
				image.width = loader.new_height * aspect_ratio;
				image.height = loader.new_height;}
			if (loader.complete) loader.complete (image);
		}
	}
}
